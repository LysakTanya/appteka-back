﻿using Appteka.Data.Entities;
using Microsoft.EntityFrameworkCore;

#nullable disable

namespace Appteka.Data
{
    public partial class ApptekaDbContext : DbContext
    {
        public ApptekaDbContext()
        {
        }

        public ApptekaDbContext(DbContextOptions<ApptekaDbContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Category> Categories { get; set; }
        public virtual DbSet<Customer> Customers { get; set; }
        public virtual DbSet<Decommission> Decommissions { get; set; }
        public virtual DbSet<Instruction> Instructions { get; set; }
        public virtual DbSet<Medicine> Medicines { get; set; }
        public virtual DbSet<MedicineOrder> MedicineOrders { get; set; }
        public virtual DbSet<MedicineStorage> MedicineStorages { get; set; }
        public virtual DbSet<MedicineSupply> MedicineSupplies { get; set; }
        public virtual DbSet<Order> Orders { get; set; }
        public virtual DbSet<Shift> Shifts { get; set; }
        public virtual DbSet<Storage> Storages { get; set; }
        public virtual DbSet<Supplier> Suppliers { get; set; }
        public virtual DbSet<Supply> Supplies { get; set; }
        public virtual DbSet<Worker> Workers { get; set; }
        public virtual DbSet<WorkerType> WorkerTypes { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.HasAnnotation("Relational:Collation", "Cyrillic_General_CI_AS");

            modelBuilder.Entity<Category>(entity =>
            {
                entity.ToTable("Category");

                entity.Property(e => e.Measure)
                    .IsRequired()
                    .HasMaxLength(20);

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(50);
            });

            modelBuilder.Entity<Customer>(entity =>
            {
                entity.ToTable("Customer");

                entity.HasIndex(e => e.CardNumber, "NIX_Customer_CardNumber")
                    .IsUnique();

                entity.HasIndex(e => e.PhoneNumber, "NIX_Customer_PhoneNumber")
                    .IsUnique();

                entity.HasIndex(e => e.PhoneNumber, "UQ__Customer__85FB4E387AEE6A61")
                    .IsUnique();

                entity.HasIndex(e => e.CardNumber, "UQ__Customer__A4E9FFE954CD9D7B")
                    .IsUnique();

                entity.Property(e => e.BirthDate).HasColumnType("date");

                entity.Property(e => e.CardNumber)
                    .IsRequired()
                    .HasMaxLength(10)
                    .IsFixedLength(true);

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.PhoneNumber)
                    .IsRequired()
                    .HasMaxLength(13)
                    .IsFixedLength(true);

                entity.Property(e => e.Surname)
                    .IsRequired()
                    .HasMaxLength(50);
            });

            modelBuilder.Entity<Decommission>(entity =>
            {
                entity.ToTable("Decommission");

                entity.Property(e => e.Date).HasColumnType("smalldatetime");

                entity.HasMany(d => d.MedicineDecommissions)
                    .WithOne(p => p.Decommission)
                    .HasForeignKey(d => d.DecommissionId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__Decommiss__Medic__5441852A");

                entity.HasOne(d => d.Storage)
                    .WithMany(p => p.Decommissions)
                    .HasForeignKey(d => d.StorageId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__Decommiss__Stora__5535A963");
            });

            modelBuilder.Entity<Instruction>(entity =>
            {
                entity.ToTable("Instruction");

                entity.Property(e => e.Id).ValueGeneratedNever();

                entity.Property(e => e.Dosing)
                    .IsRequired()
                    .HasMaxLength(2000);

                entity.Property(e => e.Interactions)
                    .IsRequired()
                    .HasMaxLength(2000);

                entity.Property(e => e.Precautions)
                    .IsRequired()
                    .HasMaxLength(2000);

                entity.Property(e => e.ProperUse)
                    .IsRequired()
                    .HasMaxLength(2000);

                entity.Property(e => e.SideEffects)
                    .IsRequired()
                    .HasMaxLength(2000);

                entity.Property(e => e.Storage)
                    .IsRequired()
                    .HasMaxLength(1000);

                entity.HasOne(d => d.IdNavigation)
                    .WithOne(p => p.Instruction)
                    .HasForeignKey<Instruction>(d => d.Id)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__Instruction__Id__2C3393D0");
            });

            modelBuilder.Entity<Medicine>(entity =>
            {
                entity.ToTable("Medicine");

                entity.HasIndex(e => e.Code, "NIX_Medicine_Code")
                    .IsUnique();

                entity.HasIndex(e => e.Name, "NIX_Medicine_Name");

                entity.Property(e => e.Code)
                    .IsRequired()
                    .HasMaxLength(13)
                    .IsUnicode(false)
                    .IsFixedLength(true);

                entity.Property(e => e.IsActual)
                    .IsRequired();

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.HasOne(d => d.Category)
                    .WithMany(p => p.Medicines)
                    .HasForeignKey(d => d.CategoryId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__Medicine__Catego__29572725");

                entity.HasMany(m => m.MedicineDecommissions)
                    .WithOne(md => md.Medicine)
                    .HasForeignKey(md => md.MedicineId);
            });

            modelBuilder.Entity<MedicineOrder>(entity =>
            {
                entity.HasKey(e => new { e.MedicineId, e.OrderId });

                entity.ToTable("MedicineOrder");

                entity.HasOne(d => d.Medicine)
                    .WithMany(p => p.MedicineOrders)
                    .HasForeignKey(d => d.MedicineId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__MedicineO__Medic__5070F446");

                entity.HasOne(d => d.Order)
                    .WithMany(p => p.MedicineOrders)
                    .HasForeignKey(d => d.OrderId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__MedicineO__Order__5165187F");
            });

            modelBuilder.Entity<MedicineStorage>(entity =>
            {
                entity.HasKey(e => new { e.MedicineId, e.StorageId });

                entity.ToTable("MedicineStorage");

                entity.HasOne(d => d.Medicine)
                    .WithMany(p => p.MedicineStorages)
                    .HasForeignKey(d => d.MedicineId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__MedicineS__Medic__44FF419A");

                entity.HasOne(d => d.Storage)
                    .WithMany(p => p.MedicineStorages)
                    .HasForeignKey(d => d.StorageId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__MedicineS__Stora__45F365D3");
            });

            modelBuilder.Entity<MedicineDecommission>(entity =>
            {
                entity.HasKey(e => new { e.MedicineId, e.DecommissionId });

                entity.ToTable("MedicineDecommission");

                entity.HasOne(d => d.Medicine)
                    .WithMany(p => p.MedicineDecommissions)
                    .HasForeignKey(d => d.MedicineId)
                    .OnDelete(DeleteBehavior.ClientSetNull);

                entity.HasOne(d => d.Decommission)
                    .WithMany(p => p.MedicineDecommissions)
                    .HasForeignKey(d => d.DecommissionId)
                    .OnDelete(DeleteBehavior.ClientSetNull);
            });

            modelBuilder.Entity<MedicineSupply>(entity =>
            {
                entity.HasKey(e => new { e.MedicineId, e.SupplyId });

                entity.ToTable("MedicineSupply");

                entity.HasOne(d => d.Medicine)
                    .WithMany(p => p.MedicineSupplies)
                    .HasForeignKey(d => d.MedicineId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__MedicineS__Medic__412EB0B6");

                entity.HasOne(d => d.Supply)
                    .WithMany(p => p.MedicineSupplies)
                    .HasForeignKey(d => d.SupplyId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__MedicineS__Suppl__4222D4EF");
            });

            modelBuilder.Entity<Order>(entity =>
            {
                entity.ToTable("Order");

                entity.Property(e => e.Date).HasColumnType("smalldatetime");

                entity.HasOne(d => d.Customer)
                    .WithMany(p => p.Orders)
                    .HasForeignKey(d => d.CustomerId)
                    .HasConstraintName("FK__Order__CustomerI__4CA06362");

                entity.HasOne(d => d.Pharmacist)
                    .WithMany(p => p.Orders)
                    .HasForeignKey(d => d.PharmacistId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__Order__Pharmacis__4D94879B");
            });

            modelBuilder.Entity<Shift>(entity =>
            {
                entity.ToTable("Shift");

                entity.HasIndex(e => e.WorkerId, "NIX_Shift_WorkerId");

                entity.Property(e => e.EndDate).HasColumnType("datetime");

                entity.Property(e => e.StartDate).HasColumnType("datetime");

                entity.HasOne(d => d.Worker)
                    .WithMany(p => p.Shifts)
                    .HasForeignKey(d => d.WorkerId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__Shift__WorkerId__398D8EEE");
            });

            modelBuilder.Entity<Storage>(entity =>
            {
                entity.ToTable("Storage");

                entity.Property(e => e.City)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.Street)
                    .IsRequired()
                    .HasMaxLength(50);
            });

            modelBuilder.Entity<Supplier>(entity =>
            {
                entity.ToTable("Supplier");

                entity.HasIndex(e => e.Email, "NIX_Supplier_Email")
                    .IsUnique();

                entity.HasIndex(e => e.PhoneNumber, "NIX_Supplier_PhoneNumber")
                    .IsUnique();

                entity.Property(e => e.City)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.Email)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsFixedLength(true);

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.PhoneNumber)
                    .IsRequired()
                    .HasMaxLength(13)
                    .IsUnicode(false)
                    .IsFixedLength(true);

                entity.Property(e => e.Representative)
                    .IsRequired()
                    .HasMaxLength(200);
            });

            modelBuilder.Entity<Supply>(entity =>
            {
                entity.ToTable("Supply");

                entity.Property(e => e.Date).HasColumnType("smalldatetime");

                entity.HasOne<Storage>(d => d.Storage).WithMany().HasForeignKey(d => d.StorageId)
                    .OnDelete(DeleteBehavior.Restrict);

                entity.HasOne(d => d.Supplier)
                    .WithMany(p => p.Supplies)
                    .HasForeignKey(d => d.SupplierId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__Supply__Supplier__3E52440B");
            });

            modelBuilder.Entity<Worker>(entity =>
            {
                entity.ToTable("Worker");

                entity.HasIndex(e => e.Email, "NIX_Worker_Email")
                    .IsUnique();

                entity.HasIndex(e => e.PhoneNumber, "NIX_Worker_PhoneNumber")
                    .IsUnique();

                entity.HasIndex(e => e.PhoneNumber, "UQ__Worker__85FB4E3846D44CAD")
                    .IsUnique();

                entity.HasIndex(e => e.Email, "UQ__Worker__A9D105342A5B5645")
                    .IsUnique();

                entity.Property(e => e.BirthDate).HasColumnType("date");

                entity.Property(e => e.Email)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.HireDate).HasColumnType("date");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.Password)
                    .IsRequired()
                    .HasMaxLength(2000);

                entity.Property(e => e.PhoneNumber)
                    .IsRequired()
                    .HasMaxLength(13)
                    .IsUnicode(false)
                    .IsFixedLength(true);

                entity.Property(e => e.Surname)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.HasOne(d => d.Storage)
                    .WithMany(p => p.Workers)
                    .HasForeignKey(d => d.StorageId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__Worker__StorageI__34C8D9D1");

                entity.HasOne(d => d.Type)
                    .WithMany(p => p.Workers)
                    .HasForeignKey(d => d.TypeId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__Worker__TypeId__35BCFE0A");
            });

            modelBuilder.Entity<WorkerType>(entity =>
            {
                entity.ToTable("WorkerType");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(50);
            });

        }

    }
}
