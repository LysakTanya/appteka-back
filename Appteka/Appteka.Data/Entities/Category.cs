﻿using System.Collections.Generic;
using Appteka.Data.Infrastructure;

#nullable disable

namespace Appteka.Data.Entities
{
    public partial class Category : IEntity
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Measure { get; set; }

        public virtual ICollection<Medicine> Medicines { get; set; } = new HashSet<Medicine>();
    }
}
