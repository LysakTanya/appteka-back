﻿using System;
using System.Collections.Generic;
using Appteka.Data.Infrastructure;

namespace Appteka.Data.Entities
{
    public class Decommission : IEntity
    {
        public int Id { get; set; }
        public int StorageId { get; set; }
        public DateTime Date { get; set; }
        public decimal Price { get; set; }

        public virtual ICollection<MedicineDecommission> MedicineDecommissions { get; set; }
        public virtual Storage Storage { get; set; }
    }
}
