﻿using System.Collections.Generic;
using Appteka.Data.Infrastructure;

namespace Appteka.Data.Entities
{
    public class Medicine : IEntity
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int CategoryId { get; set; }
        public int Quantity { get; set; }
        public decimal Price { get; set; }
        public string Code { get; set; }
        public bool IsActual { get; set; }

        public virtual Category Category { get; set; }
        public virtual Instruction Instruction { get; set; }
        public virtual ICollection<MedicineOrder> MedicineOrders { get; set; } = new HashSet<MedicineOrder>();
        public virtual ICollection<MedicineStorage> MedicineStorages { get; set; } = new HashSet<MedicineStorage>();
        public virtual ICollection<MedicineSupply> MedicineSupplies { get; set; } = new HashSet<MedicineSupply>();
        public virtual ICollection<MedicineDecommission> MedicineDecommissions { get; set; } = new HashSet<MedicineDecommission>();
    }
}
