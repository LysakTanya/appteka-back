﻿namespace Appteka.Data.Entities
{
    public class MedicineStorage
    {
        public int MedicineId { get; set; }
        public int StorageId { get; set; }
        public int Count { get; set; }

        public virtual Medicine Medicine { get; set; }
        public virtual Storage Storage { get; set; }
    }
}
