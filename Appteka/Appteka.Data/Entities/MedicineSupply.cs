﻿namespace Appteka.Data.Entities
{
    public class MedicineSupply
    {
        public int MedicineId { get; set; }
        public int SupplyId { get; set; }
        public int Count { get; set; }
        public decimal Price { get; set; }

        public virtual Medicine Medicine { get; set; }
        public virtual Supply Supply { get; set; }
    }
}
