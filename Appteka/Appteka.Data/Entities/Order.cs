﻿using System;
using System.Collections.Generic;
using Appteka.Data.Infrastructure;

namespace Appteka.Data.Entities
{
    public class Order : IEntity
    {
        public int Id { get; set; }
        public int? CustomerId { get; set; }
        public decimal Price { get; set; }
        public int PharmacistId { get; set; }
        public DateTime Date { get; set; }
        public int StorageId { get; set; }

        public virtual Customer Customer { get; set; }
        public virtual Worker Pharmacist { get; set; }
        public virtual Storage Storage { get; set; }
        public virtual ICollection<MedicineOrder> MedicineOrders { get; set; } = new HashSet<MedicineOrder>();
    }
}
