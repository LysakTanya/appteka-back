﻿using System;
using Appteka.Data.Infrastructure;

namespace Appteka.Data.Entities
{
    public class Shift : IEntity
    {
        public int Id { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public bool IsDouble { get; set; }
        public int WorkerId { get; set; }

        public virtual Worker Worker { get; set; }
    }
}
