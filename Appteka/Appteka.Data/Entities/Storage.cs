﻿using System.Collections.Generic;
using Appteka.Data.Infrastructure;

namespace Appteka.Data.Entities
{
    public class Storage : IEntity
    {
        public int Id { get; set; }
        public string City { get; set; }
        public string Street { get; set; }
        public int Number { get; set; }

        public virtual ICollection<Decommission> Decommissions { get; set; } = new HashSet<Decommission>();
        public virtual ICollection<MedicineStorage> MedicineStorages { get; set; } = new HashSet<MedicineStorage>();
        public virtual ICollection<Worker> Workers { get; set; } = new HashSet<Worker>();
    }
}
