﻿using System.Collections.Generic;
using Appteka.Data.Infrastructure;

namespace Appteka.Data.Entities
{
    public class Supplier : IEntity
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Representative { get; set; }
        public string PhoneNumber { get; set; }
        public string Email { get; set; }
        public string City { get; set; }

        public virtual ICollection<Supply> Supplies { get; set; } = new HashSet<Supply>();
    }
}
