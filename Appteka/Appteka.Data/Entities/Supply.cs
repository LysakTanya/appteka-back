﻿using System;
using System.Collections.Generic;
using Appteka.Data.Infrastructure;

namespace Appteka.Data.Entities
{
    public class Supply : IEntity
    {
        public int Id { get; set; }
        public DateTime Date { get; set; }
        public decimal Price { get; set; }
        public int SupplierId { get; set; }
        public int StorageId { get; set; }

        public virtual Storage Storage { get; set; }
        public virtual Supplier Supplier { get; set; }
        public virtual ICollection<MedicineSupply> MedicineSupplies { get; set; } = new HashSet<MedicineSupply>();
    }
}
