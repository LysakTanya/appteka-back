﻿using System;
using System.Collections.Generic;
using Appteka.Data.Infrastructure;

namespace Appteka.Data.Entities
{
    public class Worker : IEntity
    {
        public int Id { get; set; }
        public int TypeId { get; set; }
        public string Name { get; set; }
        public string Surname { get; set; }
        public DateTime BirthDate { get; set; }
        public DateTime HireDate { get; set; }
        public string Email { get; set; }
        public string PhoneNumber { get; set; }
        public int StorageId { get; set; }
        public string Password { get; set; }

        public virtual Storage Storage { get; set; }
        public virtual WorkerType Type { get; set; }
        public virtual ICollection<Order> Orders { get; set; } = new HashSet<Order>();
        public virtual ICollection<Shift> Shifts { get; set; } = new HashSet<Shift>();
    }
}
