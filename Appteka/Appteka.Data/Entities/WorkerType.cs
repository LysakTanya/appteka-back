﻿using System.Collections.Generic;
using Appteka.Data.Infrastructure;

namespace Appteka.Data.Entities
{
    public class WorkerType : IEntity
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public decimal Salary { get; set; }

        public virtual ICollection<Worker> Workers { get; set; } = new HashSet<Worker>();
    }
}
