﻿namespace Appteka.Data.Infrastructure
{
    public interface IEntity
    {
        int Id { get; set; }
    }
}
