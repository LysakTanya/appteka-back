﻿using System.Linq;
using System.Threading.Tasks;
using Appteka.Data.Infrastructure;
using Microsoft.EntityFrameworkCore.Storage;

namespace Appteka.Data
{
    public interface IRepository<TEntity> 
        where TEntity: class, IEntity
    {
        Task<TEntity> AddAsync(TEntity entity);
        Task<TEntity> UpdateAsync(TEntity entity);
        ValueTask<TEntity> GetByKeysAsync(params object[] keys);
        Task DeleteAsync(int id);
        IQueryable<TEntity> Collection { get; }
        Task<int> SaveChangesAsync();
        Task<IDbContextTransaction> BeginTransactionAsync();
    }
}
