﻿using System.Linq;
using System.Threading.Tasks;

namespace Appteka.Data.Infrastructure
{
    public interface IRepository<TEntity> 
        where TEntity: class, IEntity
    {
        Task<TEntity> AddAsync(TEntity entity);
        Task<TEntity> UpdateAsync(TEntity entity);
        ValueTask<TEntity> GetByKeysAsync(params object[] keys);
        Task DeleteAsync(int id);
        IQueryable<TEntity> Collection { get; }
        Task<int> SaveChangesAsync();
    }
}
