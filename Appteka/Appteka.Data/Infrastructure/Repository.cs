﻿using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace Appteka.Data.Infrastructure
{
    public class Repository<TEntity> : IRepository<TEntity>
    where TEntity : class, IEntity
    {
        private readonly ApptekaContext dbContext;
        private readonly DbSet<TEntity> entitiesSet;

        public Repository(ApptekaContext dbContext)
        {
            this.dbContext = dbContext;
            entitiesSet = dbContext.Set<TEntity>();
        }

        public async Task<TEntity> AddAsync(TEntity entity) => (await entitiesSet.AddAsync(entity)).Entity;

        public Task<TEntity> UpdateAsync(TEntity entity) => Task.Run(() => entitiesSet.Update(entity).Entity);

        public ValueTask<TEntity> GetByKeysAsync(params object[] keys) => entitiesSet.FindAsync(keys);

        public async Task DeleteAsync(int id)
        {
            TEntity entityToDelete = await entitiesSet.FindAsync(id);
            entitiesSet.Remove(entityToDelete);
        }

        public IQueryable<TEntity> Collection => entitiesSet;

        public Task<int> SaveChangesAsync() => dbContext.SaveChangesAsync();
    }
}
