﻿namespace Appteka.Data
{
    public enum Role
    {
        Admin = 1,
        Pharmacist = 2,
        Accountant = 3,
        Manager = 4,
    }
}
