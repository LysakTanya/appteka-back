﻿using Appteka.Data.Entities;
using Appteka.Domain.Models.CustomerModels;
using AutoMapper;

namespace Appteka.Domain.Mappers
{
    public class CustomerMapper :Profile
    {
        public CustomerMapper()
        {
            CreateMap<Customer, CustomerModel>().ReverseMap();
        }
    }
}
