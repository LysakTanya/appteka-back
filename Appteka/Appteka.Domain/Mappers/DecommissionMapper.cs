﻿using Appteka.Data.Entities;
using Appteka.Domain.Models.DecommissionModels;
using AutoMapper;

namespace Appteka.Domain.Mappers
{
    public class DecommissionMapper : Profile
    {
        public DecommissionMapper()
        {
            CreateMap<Decommission, DecommissionModel>().ReverseMap();
            CreateMap<MedicineDecommission, MedicineDecommissionModel>().ReverseMap();
            CreateMap<CreateMedicineDecommissionModel, MedicineDecommission>();
            CreateMap<CreateDecommissionModel, Decommission>();
        }
    }
}
