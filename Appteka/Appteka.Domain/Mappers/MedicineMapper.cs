﻿using Appteka.Data.Entities;
using Appteka.Domain.Models;
using Appteka.Domain.Models.MedicineModels;
using AutoMapper;

namespace Appteka.Domain.Mappers
{
    public class MedicineMapper : Profile
    {
        public MedicineMapper()
        {
            CreateMap<CreateMedicineModel, Medicine>();
            CreateMap<MedicineModel, Medicine>().ReverseMap();
            CreateMap<UpdateMedicineModel, Medicine>();

            CreateMap<CategoryModel, Category>().ReverseMap();

            CreateMap<InstructionModel, Instruction>().ReverseMap();
        }
    }
}
