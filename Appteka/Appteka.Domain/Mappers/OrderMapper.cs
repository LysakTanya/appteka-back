﻿using Appteka.Data.Entities;
using Appteka.Domain.Models.OrderModels;
using AutoMapper;

namespace Appteka.Domain.Mappers
{
    public class OrderMapper : Profile
    {
        public OrderMapper()
        {
            CreateMap<Order, OrderModel>().ReverseMap();
            CreateMap<CreateOrderModel, Order>();
            CreateMap<CreateMedicineOrderModel, MedicineOrder>();
            CreateMap<MedicineOrder, MedicineOrderModel>().ReverseMap();
        }
    }
}
