﻿using Appteka.Data.Entities;
using Appteka.Domain.Models.ShiftModels;
using AutoMapper;

namespace Appteka.Domain.Mappers
{
    public class ShiftMapper : Profile
    {
        public ShiftMapper()
        {
            CreateMap<Shift, ShiftModel>().ReverseMap();
            CreateMap<CreateShiftModel, Shift>();
        }
    }
}
