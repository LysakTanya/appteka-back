﻿using Appteka.Data.Entities;
using Appteka.Domain.Models.MedicineModels;
using Appteka.Domain.Models.StorageModels;
using AutoMapper;

namespace Appteka.Domain.Mappers
{
    public class StorageMapper: Profile
    {
        public StorageMapper()
        {
            CreateMap<Storage, StorageModel>().ReverseMap();
            CreateMap<MedicineStorage, StorageMedicineModel>();
            CreateMap<Storage, StorageInfoModel>().ForMember(s => s.Medicines,
                expression => expression.MapFrom(s => s.MedicineStorages));
            CreateMap<MedicineStorage, MedicineStorageModel>();
        }
    }
}
