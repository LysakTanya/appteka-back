﻿using Appteka.Data.Entities;
using Appteka.Domain.Models.SupplierModels;
using AutoMapper;

namespace Appteka.Domain.Mappers
{
    public class SupplierMapper : Profile
    {
        public SupplierMapper()
        {
            CreateMap<Supplier, SupplierModel>().ReverseMap();
        }
    }
}
