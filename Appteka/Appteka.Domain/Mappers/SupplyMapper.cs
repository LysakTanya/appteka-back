﻿using Appteka.Data.Entities;
using Appteka.Domain.Models.SupplyModels;
using AutoMapper;

namespace Appteka.Domain.Mappers
{
    public class SupplyMapper : Profile
    {
        public SupplyMapper()
        {
            CreateMap<MedicineSupplyModel, MedicineSupply>().ReverseMap();
            CreateMap<Supply, SupplyModel>().ForMember(s => s.Medicines, 
                    o => o.MapFrom(s => s.MedicineSupplies))
                .ReverseMap();
            CreateMap<CreateSupplyModel, Supply>();
            CreateMap<CreateMedicineSupplyModel, MedicineSupply>();
        }
    }
}
