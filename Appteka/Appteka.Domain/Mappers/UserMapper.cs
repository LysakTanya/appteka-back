﻿using Appteka.Data.Entities;
using Appteka.Domain.Models.UserModels;
using AutoMapper;

namespace Appteka.Domain.Mappers
{
    public class UserMapper: Profile
    {
        public UserMapper()
        {
            CreateMap<Worker, AuthenticatedUserModel>();
        }
    }
}
