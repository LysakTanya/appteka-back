﻿using Appteka.Data.Entities;
using Appteka.Domain.Models;
using Appteka.Domain.Models.WorkerModels;
using AutoMapper;

namespace Appteka.Domain.Mappers
{
    public class WorkerMapper : Profile
    {
        public WorkerMapper()
        {
            CreateMap<Worker, WorkerModel>().ReverseMap();
            CreateMap<WorkerType, WorkerTypeModel>().ReverseMap();
            CreateMap<CreateWorkerModel, Worker>();
            CreateMap<UpdateWorkerModel, Worker>();
        }
    }
}
