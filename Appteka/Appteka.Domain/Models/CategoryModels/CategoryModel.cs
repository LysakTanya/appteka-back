﻿namespace Appteka.Domain.Models
{
    public class CategoryModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Measure { get; set; }
    }
}
