﻿using System.Collections.Generic;

namespace Appteka.Domain.Models.DecommissionModels
{
    public class CreateDecommissionModel
    {
        public ICollection<CreateMedicineDecommissionModel> MedicineDecommissions { get; set; }
    }
}
