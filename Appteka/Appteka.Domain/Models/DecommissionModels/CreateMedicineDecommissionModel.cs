﻿namespace Appteka.Domain.Models.DecommissionModels
{
    public class CreateMedicineDecommissionModel
    {
        public int Count { get; set; }
        public decimal Price { get; set; }
        public int MedicineId { get; set; }
    }
}
