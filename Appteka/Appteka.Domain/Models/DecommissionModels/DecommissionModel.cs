﻿using System;
using System.Collections.Generic;
using Appteka.Domain.Models.StorageModels;

namespace Appteka.Domain.Models.DecommissionModels
{
    public class DecommissionModel
    {
        public int Id { get; set; }
        public DateTime Date { get; set; }
        public decimal Price { get; set; }

        public  ICollection<MedicineDecommissionModel> MedicineDecommissions { get; set; }
        public  StorageModel Storage { get; set; }
    }
}
