﻿using Appteka.Domain.Models.MedicineModels;

namespace Appteka.Domain.Models.DecommissionModels
{
    public class MedicineDecommissionModel
    {
        public decimal Price { get; set; }
        public int Count { get; set; }

        public MedicineModel Medicine { get; set; }
    }
}
