﻿namespace Appteka.Domain.Models.MedicineModels
{
    public class CreateMedicineModel
    {
        public string Name { get; set; }
        public int CategoryId { get; set; }
        public int Quantity { get; set; }
        public decimal Price { get; set; }
        public string Code { get; set; }
        public bool IsActual { get; set; } = true;
        public InstructionModel Instruction { get; set; }
    }
}
