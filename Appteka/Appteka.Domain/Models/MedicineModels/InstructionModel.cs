﻿namespace Appteka.Domain.Models.MedicineModels
{
    public class InstructionModel
    {
        public int Id { get; set; }
        public string Interactions { get; set; }
        public string Dosing { get; set; }
        public string Storage { get; set; }
        public string Precautions { get; set; }
        public string SideEffects { get; set; }
        public string ProperUse { get; set; }
    }
}
