﻿namespace Appteka.Domain.Models.MedicineModels
{
    public class MedicineModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public CategoryModel Category { get; set; }
        public int Quantity { get; set; }
        public decimal Price { get; set; }
        public string Code { get; set; }
        public bool IsActual { get; set; }
        public InstructionModel Instruction { get; set; }
    }
}
