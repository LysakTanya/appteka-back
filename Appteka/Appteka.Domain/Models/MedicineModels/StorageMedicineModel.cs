﻿namespace Appteka.Domain.Models.MedicineModels
{
    public class StorageMedicineModel
    {
        public MedicineModel Medicine { get; set; }
        public int Count { get; set; }
    }
}
