﻿namespace Appteka.Domain.Models.OrderModels
{
    public class CreateMedicineOrderModel
    {
        public int Count { get; set; }
        public decimal Price { get; set; }
        public int MedicineId { get; set; }
    }
}
