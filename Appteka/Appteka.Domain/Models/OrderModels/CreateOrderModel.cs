﻿using System.Collections.Generic;

namespace Appteka.Domain.Models.OrderModels
{
    public class CreateOrderModel
    {
        public int? CustomerId { get; set; }
        public ICollection<CreateMedicineOrderModel> MedicineOrders { get; set; }
    }
}
