﻿using Appteka.Domain.Models.MedicineModels;

namespace Appteka.Domain.Models.OrderModels
{
    public class MedicineOrderModel
    {
        public int Count { get; set; }
        public decimal Price { get; set; }
        public MedicineModel Medicine { get; set; }
    }
}
