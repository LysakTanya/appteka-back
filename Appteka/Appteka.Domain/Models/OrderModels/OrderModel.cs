﻿using System;
using System.Collections.Generic;
using Appteka.Domain.Models.CustomerModels;
using Appteka.Domain.Models.StorageModels;

namespace Appteka.Domain.Models.OrderModels
{
    public class OrderModel
    {
        public int Id { get; set; }
        public CustomerModel Customer { get; set; }
        public decimal Price { get; set; }
        public DateTime Date { get; set; }
        public WorkerModel Pharmacist { get; set; }
        public StorageModel Storage { get; set; }
        public ICollection<MedicineOrderModel> MedicineOrders { get; set; }
    }
}
