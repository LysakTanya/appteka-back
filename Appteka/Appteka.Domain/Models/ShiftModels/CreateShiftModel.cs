﻿using System;

namespace Appteka.Domain.Models.ShiftModels
{
    public class CreateShiftModel
    {
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public bool IsDouble { get; set; }
        public int WorkerId { get; set; }
    }
}
