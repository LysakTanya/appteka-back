﻿using System;

namespace Appteka.Domain.Models.ShiftModels
{
    public class ShiftModel
    {
        public int Id { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public bool IsDouble { get; set; }

        public WorkerModel Worker { get; set; }
    }
}
