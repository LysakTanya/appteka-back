﻿using Appteka.Domain.Models.MedicineModels;

namespace Appteka.Domain.Models.StorageModels
{
    public class MedicineStorageModel
    {
        public int Count { get; set; }
        public MedicineModel Medicine { get; set; }
    }
}
