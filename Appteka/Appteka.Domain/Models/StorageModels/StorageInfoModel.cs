﻿using System.Collections.Generic;
using Appteka.Domain.Models.MedicineModels;

namespace Appteka.Domain.Models.StorageModels
{
    public class StorageInfoModel
    {
        public int Id { get; set; }
        public string City { get; set; }
        public string Street { get; set; }
        public int Number { get; set; }

        public ICollection<WorkerModel> Workers { get; set; }
        public ICollection<StorageMedicineModel> Medicines { get; set; }
    }
}
