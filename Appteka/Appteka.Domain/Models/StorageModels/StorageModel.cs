﻿namespace Appteka.Domain.Models.StorageModels
{
    public class StorageModel
    {
        public int Id { get; set; }
        public string City { get; set; }
        public string Street { get; set; }
        public int Number { get; set; }
    }
}
