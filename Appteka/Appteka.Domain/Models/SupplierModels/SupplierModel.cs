﻿namespace Appteka.Domain.Models.SupplierModels
{
    public class SupplierModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Representative { get; set; }
        public string PhoneNumber { get; set; }
        public string Email { get; set; }
        public string City { get; set; }
    }
}
