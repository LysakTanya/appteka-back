﻿namespace Appteka.Domain.Models.SupplyModels
{
    public class CreateMedicineSupplyModel
    {
        public int MedicineId { get; set; }
        public int Count { get; set; }
        public decimal Price { get; set; }
    }
}
