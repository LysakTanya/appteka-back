﻿using System.Collections.Generic;

namespace Appteka.Domain.Models.SupplyModels
{
    public class CreateSupplyModel
    {
        public int StorageId { get; set; }
        public ICollection<CreateMedicineSupplyModel> MedicineSupplies { get; set; }
        public int SupplierId { get; set; }
    }
}
