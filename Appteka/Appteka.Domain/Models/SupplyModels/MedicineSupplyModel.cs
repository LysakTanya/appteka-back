﻿using Appteka.Domain.Models.MedicineModels;

namespace Appteka.Domain.Models.SupplyModels
{
    public class MedicineSupplyModel
    {
        public MedicineModel Medicine { get; set; }
        public int Count { get; set; }
        public decimal Price { get; set; }
    }
}
