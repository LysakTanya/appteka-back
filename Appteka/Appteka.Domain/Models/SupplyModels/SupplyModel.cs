﻿using System;
using System.Collections.Generic;
using Appteka.Domain.Models.StorageModels;
using Appteka.Domain.Models.SupplierModels;

namespace Appteka.Domain.Models.SupplyModels
{
    public class SupplyModel
    {
        public int Id { get; set; }
        public DateTime Date { get; set; }
        public decimal Price { get; set; }

        public StorageModel Storage { get; set; }
        public ICollection<MedicineSupplyModel> Medicines { get; set; } = new List<MedicineSupplyModel>();
        public SupplierModel Supplier { get; set; }
    }
}
