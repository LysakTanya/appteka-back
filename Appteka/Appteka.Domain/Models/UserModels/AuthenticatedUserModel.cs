﻿using System;

namespace Appteka.Domain.Models.UserModels
{
    public class AuthenticatedUserModel
    {
        public int Id { get; set; }
        public int RoleId { get; set; }
        public string Role { get; set; }
        public string Name { get; set; }
        public string Surname { get; set; }
        public DateTime BirthDate { get; set; }
        public DateTime HireDate { get; set; }
        public string Email { get; set; }
        public string PhoneNumber { get; set; }
        public int StorageId { get; set; }
        public string Token { get; set; }
    }
}
