﻿using System;

namespace Appteka.Domain.Models.WorkerModels
{
    public class CreateWorkerModel
    {
        public int TypeId { get; set; }
        public string Name { get; set; }
        public string Surname { get; set; }
        public DateTime BirthDate { get; set; }
        public DateTime HireDate { get; set; }
        public string Email { get; set; }
        public string PhoneNumber { get; set; }
        public int StorageId { get; set; }
        public string Password { get; set; }
    }
}
