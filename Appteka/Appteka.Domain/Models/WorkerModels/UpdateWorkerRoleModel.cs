﻿namespace Appteka.Domain.Models.WorkerModels
{
    public class UpdateWorkerRoleModel
    {
        public int WorkerId { get; set; }
        public int RoleId { get; set; }
    }
}
