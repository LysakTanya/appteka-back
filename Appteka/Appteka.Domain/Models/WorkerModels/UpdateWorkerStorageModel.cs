﻿namespace Appteka.Domain.Models.WorkerModels
{
    public class UpdateWorkerStorageModel
    {
        public int WorkerId { get; set; }
        public int StorageId { get; set; }
    }
}
