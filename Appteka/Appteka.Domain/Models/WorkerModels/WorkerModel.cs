﻿using System;

namespace Appteka.Domain.Models
{
    public class WorkerModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Surname { get; set; }
        public DateTime BirthDate { get; set; }
        public DateTime HireDate { get; set; }
        public string Email { get; set; }
        public string PhoneNumber { get; set; }
        public int StorageId { get; set; }
        public WorkerTypeModel Type { get; set; }
    }
}
