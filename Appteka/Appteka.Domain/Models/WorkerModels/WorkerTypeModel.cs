﻿namespace Appteka.Domain.Models
{
    public class WorkerTypeModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public decimal Salary { get; set; }
    }
}
