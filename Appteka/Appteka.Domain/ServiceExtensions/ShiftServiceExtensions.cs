﻿using System;
using System.Linq;
using Appteka.Data.Entities;

namespace Appteka.Domain.ServiceExtensions
{
    public static class ShiftServiceExtensions
    {
        public  static IQueryable<Shift> GetShiftForWeek(this IQueryable<Shift> shifts, int weekOffset)
        {
            const int daysPerWeek = 7;
            var now = DateTime.Now;
            var dayOfWeek = (int)now.DayOfWeek;
            var startOfWeek = now.DayOfYear - dayOfWeek + 1 + daysPerWeek * weekOffset;
            var endOfWeek = now.DayOfYear + (daysPerWeek - dayOfWeek) + daysPerWeek * weekOffset;

            return shifts.Where(shift => shift.StartDate.DayOfYear >= startOfWeek
                                         && shift.StartDate.DayOfYear <= endOfWeek);
        }
    }
}
