﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Appteka.Data;
using Appteka.Data.Entities;
using Appteka.Domain.Models;
using Appteka.Domain.Services.Interfaces;
using AutoMapper;
using Microsoft.EntityFrameworkCore;

namespace Appteka.Domain.Services.Implementation
{
    public class CategoryService : ICategoryService
    {
        private readonly IRepository<Category> categoryRepository;
        private readonly IMapper mapper;

        public CategoryService(IRepository<Category> categoryRepository, IMapper mapper)
        {
            this.categoryRepository = categoryRepository;
            this.mapper = mapper;
        }

        public async Task<CategoryModel> AddCategoryAsync(CategoryModel categoryModel)
        {
            var category = mapper.Map<CategoryModel, Category>(categoryModel);
            var newCategory = await categoryRepository.AddAsync(category);
            await categoryRepository.SaveChangesAsync();

            return mapper.Map<Category, CategoryModel>(newCategory);
        }

        public async Task<CategoryModel> UpdateCategoryAsync(CategoryModel categoryModel)
        {
            var category = mapper.Map<CategoryModel, Category>(categoryModel);
            var updatedCategory = await categoryRepository.UpdateAsync(category);
            await categoryRepository.SaveChangesAsync();

            return mapper.Map<Category, CategoryModel>(updatedCategory);
        }

        public async Task<List<CategoryModel>> GetAllCategoriesAsync()
        {
            var categories = await categoryRepository.Collection.ToListAsync();
            return mapper.Map<List<Category>, List<CategoryModel>>(categories);
        }
    }
}
