﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Appteka.Data;
using Appteka.Data.Entities;
using Appteka.Domain.Models.CustomerModels;
using Appteka.Domain.Services.Interfaces;
using AutoMapper;
using Microsoft.EntityFrameworkCore;

namespace Appteka.Domain.Services.Implementation
{
    public class CustomerService : ICustomerService
    {
        private readonly IRepository<Customer> customerRepository;
        private readonly IMapper mapper;

        public CustomerService(IRepository<Customer> customerRepository, IMapper mapper)
        {
            this.customerRepository = customerRepository;
            this.mapper = mapper;
        }

        public async Task<CustomerModel> GetCustomerByPhoneNumberAsync(string number)
        {
            var customer = await customerRepository.Collection.FirstOrDefaultAsync(c => c.PhoneNumber == "+" + number);
            return mapper.Map<Customer, CustomerModel>(customer);
        }

        public async Task<CustomerModel> GetCustomerByCardNumberAsync(string number)
        {
            var customer = await customerRepository.Collection.FirstOrDefaultAsync(c => c.CardNumber == number);
            return mapper.Map<Customer, CustomerModel>(customer);
        }

        public async Task<CustomerModel> AddCustomerAsync(CustomerModel customerModel)
        {
            var customer = mapper.Map<CustomerModel, Customer>(customerModel);
            var newCustomer = await customerRepository.AddAsync(customer);
            await customerRepository.SaveChangesAsync();

            return mapper.Map<Customer, CustomerModel>(newCustomer);
        }

        public async Task<List<CustomerModel>> GetCustomersAsync()
        {
            var customers = await customerRepository.Collection.ToListAsync();
            return mapper.Map<List<Customer>, List<CustomerModel>>(customers);
        }
    }
}
