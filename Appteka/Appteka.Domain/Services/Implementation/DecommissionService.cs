﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Appteka.Data;
using Appteka.Data.Entities;
using Appteka.Domain.Models.DecommissionModels;
using Appteka.Domain.Services.Interfaces;
using AutoMapper;
using Microsoft.EntityFrameworkCore;

namespace Appteka.Domain.Services.Implementation
{
    public class DecommissionService : IDecommissionService
    {
        private readonly IRepository<Decommission> decommissionRepository;
        private readonly IRepository<Storage> storageRepository;
        private readonly IMapper mapper;

        public DecommissionService(IRepository<Decommission> decommissionRepository, IRepository<Storage> storageRepository, IMapper mapper)
        {
            this.mapper = mapper;
            this.storageRepository = storageRepository;
            this.decommissionRepository = decommissionRepository;
        }

        public async Task<DecommissionModel> AddDecommissionAsync(CreateDecommissionModel createDecommissionModel, int storageId)
        {
            var storage = await storageRepository.GetByKeysAsync(storageId);
            var decommission = mapper.Map<CreateDecommissionModel, Decommission>(createDecommissionModel);
            decommission.Date = DateTime.Now;
            decommission.Price = createDecommissionModel.MedicineDecommissions.Sum(md => md.Count * md.Price);
            decommission.StorageId = storageId;

            var transaction = await decommissionRepository.BeginTransactionAsync();
            try
            {
                var newDecommission = await decommissionRepository.AddAsync(decommission);
                foreach (var medicineDecommission in decommission.MedicineDecommissions)
                {
                    var medicine = storage.MedicineStorages.First(ms => ms.MedicineId == medicineDecommission.MedicineId);
                    if (medicine.Count < medicineDecommission.Count)
                    {
                        throw new Exception("Неможливо списати товари.");
                    }

                    medicine.Count -= medicineDecommission.Count;
                }

                await decommissionRepository.SaveChangesAsync();
                await transaction.CommitAsync();

                newDecommission = await decommissionRepository.Collection
                    .Include(d => d.MedicineDecommissions)
                    .ThenInclude(md => md.Medicine)
                    .ThenInclude(m => m.Category)
                    .Include(d => d.Storage)
                    .FirstAsync(d => d.Id == newDecommission.Id);
                return mapper.Map<Decommission, DecommissionModel>(newDecommission);
            }
            catch (Exception)
            {
                await transaction.RollbackAsync();
                return null;
            }
            finally
            {
                await transaction.DisposeAsync();
            }
        }

        public async Task<List<DecommissionModel>> GetAllDecommissions(int storageId = 0)
        {
            List<Decommission> decommissions;
            if (storageId == 0)
            {
                decommissions = await decommissionRepository.Collection.ToListAsync();
            }
            else
            {
                decommissions = await decommissionRepository.Collection.Where(d => d.StorageId == storageId)
                    .ToListAsync();
            }

            return mapper.Map<List<Decommission>, List<DecommissionModel>>(decommissions);
        }
    }
}
