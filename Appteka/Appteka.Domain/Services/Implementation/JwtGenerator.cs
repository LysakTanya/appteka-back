﻿using System;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using Appteka.Data;
using Appteka.Data.Entities;
using Appteka.Domain.Configurations;
using Appteka.Domain.Services.Interfaces;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;

namespace Appteka.Domain.Services.Implementation
{
    public class JwtGenerator : IWebTokenGenerator
    {
        private readonly IOptions<Jwt> jwtOptions;

        public JwtGenerator(IOptions<Jwt> jwtOptions)
        {
            this.jwtOptions = jwtOptions;
        }

        public string GenerateWebToken(Worker worker)
        {
            var securityKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(jwtOptions.Value.Key));
            var credentials = new SigningCredentials(securityKey, SecurityAlgorithms.HmacSha256);

            Role role = (Role)worker.Type.Id;

            var claims = new[]
            {
                new Claim(ClaimTypes.Name, worker.Id.ToString()), 
                new Claim(ClaimTypes.Email, worker.Email),
                new Claim(ClaimTypes.Role, role.ToString()),
                new Claim("StorageId", worker.StorageId.ToString()),
            };

            var now = DateTime.Now;
            var token = new JwtSecurityToken(
                jwtOptions.Value.Issuer,
                jwtOptions.Value.Issuer,
                claims,
                signingCredentials: credentials,
                expires: now.AddDays(2));

            return new JwtSecurityTokenHandler().WriteToken(token);
        }
    }
}
