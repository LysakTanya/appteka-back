﻿using System.Threading.Tasks;
using Appteka.Data;
using Appteka.Data.Entities;
using Appteka.Domain.Models.UserModels;
using Appteka.Domain.Services.Interfaces;
using AutoMapper;
using CryptoHelper;
using Microsoft.EntityFrameworkCore;

namespace Appteka.Domain.Services.Implementation
{
    public class LoginService : ILoginService
    {
        private readonly IRepository<Worker> workerRepository;
        private readonly IWebTokenGenerator tokenGenerator;
        private readonly IMapper mapper;

        public LoginService(IRepository<Worker> workerRepository, IWebTokenGenerator tokenGenerator, IMapper mapper)
        {
            this.tokenGenerator = tokenGenerator;
            this.workerRepository = workerRepository;
            this.mapper = mapper;
        }

        public async Task<AuthenticatedUserModel> LoginAsync(LoginModel loginModel)
        {
            var worker = await workerRepository.Collection.Include(w => w.Type)
                .FirstOrDefaultAsync(w => w.Email == loginModel.Email);
            if (worker == null || !Crypto.VerifyHashedPassword(worker.Password, loginModel.Password))
            {
                return null;
            }

            var token = tokenGenerator.GenerateWebToken(worker);
            var user = mapper.Map<Worker, AuthenticatedUserModel>(worker);
            user.Token = token;
            user.Role = worker.Type.Name;
            user.RoleId = worker.Type.Id;
            return user;
        }
    }
}
