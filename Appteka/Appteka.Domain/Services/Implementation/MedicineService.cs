﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Appteka.Data;
using Appteka.Data.Entities;
using Appteka.Domain.Models.MedicineModels;
using Appteka.Domain.Services.Interfaces;
using AutoMapper;
using Microsoft.EntityFrameworkCore;

namespace Appteka.Domain.Services.Implementation
{
    public class MedicineService : IMedicineService
    {
        private readonly IRepository<Medicine> medicineRepository;
        private readonly IMapper mapper;

        public MedicineService(IRepository<Medicine> medicineRepository, IMapper mapper)
        {
            this.medicineRepository = medicineRepository;
            this.mapper = mapper;
        }

        public async Task<MedicineModel> GetMedicineByIdAsync(int id)
        {
            var medicine = await medicineRepository.Collection.Include(m => m.Category)
                .FirstOrDefaultAsync(m => m.Id == id);
            
            return mapper.Map<Medicine, MedicineModel>(medicine);
        }

        public async Task<MedicineModel> AddMedicineAsync(CreateMedicineModel createMedicineModel)
        {
            var medicine = mapper.Map<CreateMedicineModel, Medicine>(createMedicineModel);
            var addedMedicine = await medicineRepository.AddAsync(medicine);
            await medicineRepository.SaveChangesAsync();

            return mapper.Map<Medicine, MedicineModel>(addedMedicine);
        }

        public async Task<List<MedicineModel>> GetAllMedicinesAsync()
        {
            var medicines = await medicineRepository.Collection.Include(m => m.Category)
                .Include(m => m.Instruction)
                .ToListAsync();
            var medicineModels = mapper.Map<List<Medicine>, List<MedicineModel>>(medicines);

            return medicineModels;
        }

        public async Task<Medicine> MarkMedicineAsObsoleteAsync(int id)
        {
            var medicine = await medicineRepository.GetByKeysAsync(id);
            if (medicine == null) return null;
            medicine.IsActual = false;
            await medicineRepository.SaveChangesAsync();

            return medicine;
        }

        public async Task<Medicine> UpdatePriceAsync(int id, int price)
        {
            var medicine = await medicineRepository.GetByKeysAsync(id);
            if (medicine == null) return null;
            medicine.Price = price;
            await medicineRepository.SaveChangesAsync();

            return medicine;
        }

        public async Task<MedicineModel> AddInstructionAsync(InstructionModel instructionModel)
        {
            var medicine = await medicineRepository.GetByKeysAsync(instructionModel.Id);
            medicine.Instruction = mapper.Map<InstructionModel, Instruction>(instructionModel);

            await medicineRepository.SaveChangesAsync();
            return mapper.Map<Medicine, MedicineModel>(medicine);
        }

        public async Task<MedicineModel> UpdateMedicineAsync(UpdateMedicineModel updateMedicineModel)
        {
            var medicineToUpdate = mapper.Map<UpdateMedicineModel, Medicine>(updateMedicineModel);
            var updatedMedicine = await medicineRepository.UpdateAsync(medicineToUpdate);
            await medicineRepository.SaveChangesAsync();

            return mapper.Map<Medicine, MedicineModel>(updatedMedicine);
        }
    }
}
