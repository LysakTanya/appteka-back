﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Appteka.Data;
using Appteka.Data.Entities;
using Appteka.Domain.Models.OrderModels;
using Appteka.Domain.Services.Interfaces;
using AutoMapper;
using Microsoft.EntityFrameworkCore;

namespace Appteka.Domain.Services.Implementation
{
    public class OrderService : IOrderService
    {
        private readonly IRepository<Order> orderRepository;
        private readonly IRepository<Storage> storageRepository;
        private readonly IMapper mapper;

        public OrderService(IRepository<Order> orderRepository, IRepository<Storage> storageRepository, IMapper mapper)
        {
            this.orderRepository = orderRepository;
            this.storageRepository = storageRepository;
            this.mapper = mapper;
        }

        public async Task<OrderModel> AddOrderAsync(CreateOrderModel createOrderModel, int storageId, int pharmacistId)
        {
            var order = mapper.Map<CreateOrderModel, Order>(createOrderModel);
            order.Date = DateTime.Now;
            order.Price = createOrderModel.MedicineOrders.Sum(mo => mo.Count * mo.Price);
            order.PharmacistId = pharmacistId;
            order.StorageId = storageId;

            if (order.CustomerId != null)
            {
                order.Price -= order.Price * 0.05m;
            }

            var storage = await storageRepository.GetByKeysAsync(storageId);
            var transaction = await orderRepository.BeginTransactionAsync();
            try
            {
                var newOrder = await orderRepository.AddAsync(order);
                foreach (var medicineOrder in order.MedicineOrders)
                {
                    var medicineStorage = storage.MedicineStorages.First(s => s.MedicineId == medicineOrder.MedicineId);
                    medicineStorage.Count -= medicineOrder.Count;
                    if (medicineStorage.Count < 0)
                        throw new Exception("Недостатньо товарів на складі.");
                }

                await orderRepository.SaveChangesAsync();
                await transaction.CommitAsync();

                newOrder = await orderRepository.Collection
                    .Include(o => o.Storage)
                    .Include(o => o.Customer)
                    .Include(o => o.Pharmacist)
                    .Include(o => o.MedicineOrders)
                    .ThenInclude(mo => mo.Medicine)
                    .ThenInclude(mo => mo.Category)
                    .FirstOrDefaultAsync(o => o.Id == newOrder.Id);
                return mapper.Map<Order, OrderModel>(newOrder);
            }
            catch (Exception)
            {
                await transaction.RollbackAsync();
                return null;
            }
            finally
            {
                await transaction.DisposeAsync();
            }
        }

        public async Task<List<OrderModel>> GetOrdersAsync(int storageId = 0)
        {
            List<Order> orders = null;
            if (storageId == 0)
            {
                orders = await orderRepository.Collection.ToListAsync();
            }
            else
            {
                orders = await orderRepository.Collection.Where(o => o.StorageId == storageId).OrderByDescending(o => o.Date).ToListAsync();
            }

            return mapper.Map<List<Order>, List<OrderModel>>(orders);
        }
    }
}
