﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Appteka.Data;
using Appteka.Data.Entities;
using Appteka.Domain.Models;
using Appteka.Domain.Models.ShiftModels;
using Appteka.Domain.ServiceExtensions;
using Appteka.Domain.Services.Interfaces;
using AutoMapper;
using Microsoft.EntityFrameworkCore;

namespace Appteka.Domain.Services.Implementation
{
    public class ShiftService : IShiftService
    {
        private readonly IRepository<Shift> shiftRepository;
        private readonly IRepository<Worker> workerRepository;
        private readonly IMapper mapper;
        private const int MinimumHoursShiftOffset = 12;

        public ShiftService(IRepository<Shift> shiftRepository, IRepository<Worker> workerRepository, IMapper mapper)
        {
            this.shiftRepository = shiftRepository;
            this.workerRepository = workerRepository;
            this.mapper = mapper;
        }

        public async Task<ShiftModel> AddShiftAsync(CreateShiftModel createShiftModel)
        {
            var shift = mapper.Map<CreateShiftModel, Shift>(createShiftModel);
            var addedShift = await shiftRepository.AddAsync(shift);
            await shiftRepository.SaveChangesAsync();

            return mapper.Map<Shift, ShiftModel>(addedShift);
        }

        public async Task DeleteShiftAsync(int shiftId)
        {
            var shift = await shiftRepository.GetByKeysAsync(shiftId);
            if (shift.EndDate < DateTime.Now)
            {
                throw new Exception("Видалити зміну, час якої минув, неможливо.");
            }

            await shiftRepository.DeleteAsync(shift.Id);
            await shiftRepository.SaveChangesAsync();
        }

        /// <summary>
        /// Get all shifts for specified week. 
        /// </summary>
        /// <param name="relativeWeekOffset">Offset from current week (0 - current week (by default), 1 - next week, -1 - previous week)</param>
        /// <returns>List of shifts</returns>
        public async Task<List<ShiftModel>> GetShiftsForWeek(int relativeWeekOffset = 0)
        {
            var shifts = await shiftRepository.Collection.GetShiftForWeek(relativeWeekOffset).OrderBy(s => s.StartDate).ToListAsync();

            return mapper.Map<List<Shift>, List<ShiftModel>>(shifts);
        }

        public async Task<List<ShiftModel>> GetShiftsForWorker(int workerId, int weekOffset = 0)
        {
            var shifts = await shiftRepository.Collection.GetShiftForWeek(weekOffset)
                .Where(shift => shift.WorkerId == workerId).OrderBy(s => s.StartDate).ToListAsync();

            return mapper.Map<List<Shift>, List<ShiftModel>>(shifts);
        }

        public async Task<List<WorkerModel>> GetAvailableWorkersAsync(DateTime startDate, int hours, int storageId)
        {
            var workers = await workerRepository.Collection
                .Where(w => w.StorageId == storageId)
                .Where(w => w.Shifts.All(s =>
                  s.EndDate.AddHours(MinimumHoursShiftOffset) < startDate
                  || s.StartDate.AddHours(-MinimumHoursShiftOffset) > startDate.AddHours(hours)))
                .ToListAsync();

            return mapper.Map<List<Worker>, List<WorkerModel>>(workers);
        }
    }
}
