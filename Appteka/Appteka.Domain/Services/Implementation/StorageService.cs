﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Appteka.Data;
using Appteka.Data.Entities;
using Appteka.Domain.Models.StorageModels;
using Appteka.Domain.Services.Interfaces;
using AutoMapper;
using Microsoft.EntityFrameworkCore;

namespace Appteka.Domain.Services.Implementation
{
    public class StorageService : IStorageService
    {
        private readonly IRepository<Storage> storageRepository;
        private readonly IMapper mapper;

        public StorageService(IRepository<Storage> storageRepository, IMapper mapper)
        {
            this.storageRepository = storageRepository;
            this.mapper = mapper;
        }

        public async Task<StorageModel> AddStorageAsync(StorageModel storageModel)
        {
            var storage = mapper.Map<StorageModel, Storage>(storageModel);
            var newStorage = await storageRepository.AddAsync(storage);
            await storageRepository.SaveChangesAsync();

            return mapper.Map<Storage, StorageModel>(newStorage);
        }

        public async Task<StorageModel> UpdateStorageAsync(StorageModel storageModel)
        {
            var storage = mapper.Map<StorageModel, Storage>(storageModel);
            var updatedStorage = await storageRepository.UpdateAsync(storage);
            await storageRepository.SaveChangesAsync();

            return mapper.Map<Storage, StorageModel>(updatedStorage);
        }

        public async Task<IEnumerable<MedicineStorageModel>> GetMedicinesFromStorageAsync(int storageId)
        {
            var storage = await storageRepository.Collection.Include(s => s.MedicineStorages)
                .ThenInclude(ms => ms.Medicine).FirstOrDefaultAsync(s => s.Id == storageId);
            var medicines = storage.MedicineStorages.Where(m => m.Medicine.IsActual);

            return mapper.Map<IEnumerable<MedicineStorage>, IEnumerable<MedicineStorageModel>>(medicines);
        }

        public async Task<List<StorageModel>> GetAllStoragesAsync()
        {
            var storages = await storageRepository.Collection.ToListAsync();
            return mapper.Map<List<Storage>, List<StorageModel>>(storages);
        }

        public async Task<StorageInfoModel> GetStorageInfoByIdAsync(int id)
        {
            var storage = await storageRepository.Collection.Include(s => s.Workers)
                .ThenInclude(w => w.Type)
                .Include(s => s.MedicineStorages)
                .ThenInclude(s => s.Medicine)
                .ThenInclude(m => m.Category)
                .FirstOrDefaultAsync(s => s.Id == id);

            if (storage == null)
            {
                throw new Exception("Шукана торгова точка не існує.");
            }

            var storageInfo = mapper.Map<Storage, StorageInfoModel>(storage);
            return storageInfo;
        }
    }
}
