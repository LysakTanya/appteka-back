﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Appteka.Data;
using Appteka.Data.Entities;
using Appteka.Domain.Services.Interfaces;
using AutoMapper;
using Microsoft.EntityFrameworkCore;
using Appteka.Domain.Models.SupplierModels;

namespace Appteka.Domain.Services.Implementation
{
    public class SupplierService : ISupplierService
    {
        private readonly IRepository<Supplier> supplierRepository;
        private readonly IMapper mapper;

        public SupplierService(IRepository<Supplier> supplierRepository, IMapper mapper)
        {
            this.supplierRepository = supplierRepository;
            this.mapper = mapper;
        }

        public async Task<SupplierModel> AddSupplierAsync(SupplierModel supplierModel)
        {
            var supplier = mapper.Map<SupplierModel, Supplier>(supplierModel);
            var newSupplier = await supplierRepository.AddAsync(supplier);
            await supplierRepository.SaveChangesAsync();

            return mapper.Map<Supplier, SupplierModel>(newSupplier);
        }

        public async Task<List<SupplierModel>> GetAllSuppliersAsync()
        {
            var suppliers = await supplierRepository.Collection.ToListAsync();
            return mapper.Map<List<Supplier>, List<SupplierModel>>(suppliers);
        }

        public async Task<SupplierModel> UpdateSupplierAsync(SupplierModel supplierModel)
        {
            var supplier = mapper.Map<SupplierModel, Supplier>(supplierModel);
            var updatedSupplier = await supplierRepository.UpdateAsync(supplier);
            await supplierRepository.SaveChangesAsync();

            return mapper.Map<Supplier, SupplierModel>(updatedSupplier);
        }
    }
}
