﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Appteka.Data;
using Appteka.Data.Entities;
using Appteka.Domain.Models.SupplyModels;
using Appteka.Domain.Services.Interfaces;
using AutoMapper;
using Microsoft.EntityFrameworkCore;

namespace Appteka.Domain.Services.Implementation
{
    public class SupplyService : ISupplyService
    {
        private readonly IRepository<Supply> supplyRepository;
        private readonly IRepository<Storage> storageRepository;
        private readonly IMapper mapper;

        public SupplyService(IRepository<Supply> supplyRepository, IRepository<Storage> storageRepository, IMapper mapper)
        {
            this.supplyRepository = supplyRepository;
            this.storageRepository = storageRepository;
            this.mapper = mapper;
        }

        public async Task<SupplyModel> AddSupplyAsync(CreateSupplyModel createSupplyModel, int storageId)
        {
            var supply = mapper.Map<CreateSupplyModel, Supply>(createSupplyModel);
            supply.Date = DateTime.Now;
            supply.StorageId = storageId;
            supply.Price = createSupplyModel.MedicineSupplies.Sum(ms => ms.Count * ms.Price);
            var storage = await storageRepository.GetByKeysAsync(createSupplyModel.StorageId);
            var transaction = await supplyRepository.BeginTransactionAsync();
            try
            {
                var newSupply = await supplyRepository.AddAsync(supply);
                foreach (var medicineSupply in newSupply.MedicineSupplies)
                {
                    var medicineStorage =
                        storage.MedicineStorages.FirstOrDefault(ms => ms.MedicineId == medicineSupply.MedicineId);
                    if (medicineStorage != null)
                    {
                        medicineStorage.Count += medicineSupply.Count;
                    }
                    else
                    {
                        storage.MedicineStorages.Add(new MedicineStorage()
                        {
                            Count = medicineSupply.Count,
                            MedicineId = medicineSupply.MedicineId,
                            StorageId = storage.Id
                        });
                    }
                }

                await supplyRepository.SaveChangesAsync();

                await transaction.CommitAsync();
                newSupply = await supplyRepository.Collection
                    .Include(s => s.MedicineSupplies)
                    .ThenInclude(m => m.Medicine)
                    .Include(s => s.Supplier)
                    .FirstOrDefaultAsync(m => m.Id == newSupply.Id);
                return mapper.Map<Supply, SupplyModel>(newSupply);
            }
            catch (Exception)
            {
                await transaction.RollbackAsync();
                return null;
            }
            finally
            {
                await transaction.DisposeAsync();
            }
        }

        public async Task<List<SupplyModel>> GetAllSuppliesAsync(int storageId)
        {
            var supplies = await supplyRepository.Collection
                .Where(s => s.StorageId == storageId)
                .OrderByDescending(s => s.Date)
                .ToListAsync();

            return mapper.Map<List<Supply>, List<SupplyModel>>(supplies);
        }
    }
}
