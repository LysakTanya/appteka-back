﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Appteka.Data;
using Appteka.Data.Entities;
using Appteka.Domain.Models;
using Appteka.Domain.Models.WorkerModels;
using Appteka.Domain.Services.Interfaces;
using AutoMapper;
using CryptoHelper;
using Microsoft.EntityFrameworkCore;

namespace Appteka.Domain.Services.Implementation
{
    public class WorkerService : IWorkerService
    {
        private readonly IRepository<Worker> workerRepository;
        private readonly IMapper mapper;

        public WorkerService(IRepository<Worker> workerRepository, IMapper mapper)
        {
            this.workerRepository = workerRepository;
            this.mapper = mapper;
        }

        public async Task<WorkerModel> AddWorkerAsync(CreateWorkerModel createWorkerModel)
        {
            var worker = mapper.Map<CreateWorkerModel, Worker>(createWorkerModel);
            worker.Password = Crypto.HashPassword(createWorkerModel.Password);
            var newWorker = await workerRepository.AddAsync(worker);
            await workerRepository.SaveChangesAsync();

            return mapper.Map<Worker, WorkerModel>(newWorker);
        }

        public async Task<WorkerModel> UpdateWorkerAsync(UpdateWorkerModel updateWorkerModel)
        {
            var worker = mapper.Map<UpdateWorkerModel, Worker>(updateWorkerModel);
            worker.Password = Crypto.HashPassword(updateWorkerModel.Password);
            var updatedWorker = await workerRepository.UpdateAsync(worker);
            await workerRepository.SaveChangesAsync();

            return mapper.Map<Worker, WorkerModel>(updatedWorker);
        }

        public async Task<WorkerModel> UpdateWorkerRoleAsync(int workerId, int roleId)
        {
            var worker = await workerRepository.GetByKeysAsync(workerId);
            if (worker == null) return null;

            worker.TypeId = roleId;

            await workerRepository.SaveChangesAsync();
            return mapper.Map<Worker, WorkerModel>(worker);
        }

        public async Task<WorkerModel> UpdateWorkerStorageAsync(int workerId, int storageId)
        {
            var worker = await workerRepository.GetByKeysAsync(workerId);
            if (worker == null) return null;

            worker.StorageId = storageId;

            await workerRepository.SaveChangesAsync();
            return mapper.Map<Worker, WorkerModel>(worker);
        }


        public ValueTask<Worker> GetWorkerByIdAsync(int id)
        {
            return workerRepository.GetByKeysAsync(id);
        }

        public Task<List<Worker>> GetWorkersByStorageId(int storageId)
        {
            return workerRepository.Collection.Where(w => w.StorageId == storageId).ToListAsync();
        }

        public async Task<List<WorkerModel>> GetWorkersAsync()
        {
            var workers = await workerRepository.Collection.Include(w => w.Type).ToListAsync();
            return mapper.Map<List<Worker>, List<WorkerModel>>(workers);
        }
    }
}
