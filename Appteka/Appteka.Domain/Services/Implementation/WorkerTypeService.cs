﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Appteka.Data;
using Appteka.Data.Entities;
using Appteka.Domain.Models;
using Appteka.Domain.Services.Interfaces;
using AutoMapper;
using Microsoft.EntityFrameworkCore;

namespace Appteka.Domain.Services.Implementation
{
    public class WorkerTypeService : IWorkerTypeService
    {
        private readonly IRepository<WorkerType> workerTypeRepository;
        private readonly IMapper mapper;

        public WorkerTypeService(IRepository<WorkerType> workerTypeRepository, IMapper mapper)
        {
            this.workerTypeRepository = workerTypeRepository;
            this.mapper = mapper;
        }

        public async Task<WorkerTypeModel> AddWorkerTypeAsync(WorkerTypeModel workerTypeModel)
        {
            var workerType = mapper.Map<WorkerTypeModel, WorkerType>(workerTypeModel);
            var newWorkerType = await workerTypeRepository.AddAsync(workerType);
            await workerTypeRepository.SaveChangesAsync();

            return mapper.Map<WorkerType, WorkerTypeModel>(newWorkerType);
        }

        public Task<WorkerType> UpdateHourRateAsync(int workerTypeId, int hourRate)
        {
            throw new NotImplementedException();
        }

        public async Task<WorkerTypeModel> UpdateWorkerTypeAsync(WorkerTypeModel workerTypeModel)
        {
            var workerType = mapper.Map<WorkerTypeModel, WorkerType>(workerTypeModel);
            var updatedWorkerType = await workerTypeRepository.UpdateAsync(workerType);
            await workerTypeRepository.SaveChangesAsync();

            return mapper.Map<WorkerType, WorkerTypeModel>(updatedWorkerType);
        }

        public async Task<List<WorkerTypeModel>> GetAllWorkerTypes()
        {
            var workerTypes = await workerTypeRepository.Collection.ToListAsync();
            return mapper.Map<List<WorkerType>, List<WorkerTypeModel>>(workerTypes);
        }
    }
}
