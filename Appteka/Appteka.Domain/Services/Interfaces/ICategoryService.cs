﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Appteka.Domain.Models;

namespace Appteka.Domain.Services.Interfaces
{
    public interface ICategoryService
    {
        Task<CategoryModel> AddCategoryAsync(CategoryModel categoryModel);
        Task<CategoryModel> UpdateCategoryAsync(CategoryModel categoryModel);
        Task<List<CategoryModel>> GetAllCategoriesAsync();
    }
}
