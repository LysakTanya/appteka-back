﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Appteka.Domain.Models.CustomerModels;

namespace Appteka.Domain.Services.Interfaces
{
    public interface ICustomerService
    {
        Task<CustomerModel> GetCustomerByPhoneNumberAsync(string number);
        Task<CustomerModel> GetCustomerByCardNumberAsync(string number);
        Task<CustomerModel> AddCustomerAsync(CustomerModel customerModel);
        Task<List<CustomerModel>> GetCustomersAsync();
    }
}
