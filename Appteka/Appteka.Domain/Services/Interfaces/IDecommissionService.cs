﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Appteka.Domain.Models.DecommissionModels;

namespace Appteka.Domain.Services.Interfaces
{
    public interface IDecommissionService
    {
        Task<DecommissionModel> AddDecommissionAsync(CreateDecommissionModel createDecommissionModel, int storageId);
        Task<List<DecommissionModel>> GetAllDecommissions(int storageId = 0);
    }
}
