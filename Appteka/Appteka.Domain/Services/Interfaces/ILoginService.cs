﻿using System.Threading.Tasks;
using Appteka.Domain.Models.UserModels;

namespace Appteka.Domain.Services.Interfaces
{
    public interface ILoginService
    {
        Task<AuthenticatedUserModel> LoginAsync(LoginModel loginModel);
    }
}
