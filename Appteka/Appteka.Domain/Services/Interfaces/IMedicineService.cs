﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Appteka.Domain.Models.MedicineModels;

namespace Appteka.Domain.Services.Interfaces
{
    public interface IMedicineService
    {
        Task<MedicineModel> GetMedicineByIdAsync(int id);
        Task<MedicineModel> AddMedicineAsync(CreateMedicineModel createMedicineModel);
        Task<List<MedicineModel>> GetAllMedicinesAsync();
        Task<MedicineModel> AddInstructionAsync(InstructionModel instructionModel);
        Task<MedicineModel> UpdateMedicineAsync(UpdateMedicineModel updateMedicineModel);
    }
}
