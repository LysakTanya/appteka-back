﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Appteka.Domain.Models.OrderModels;

namespace Appteka.Domain.Services.Interfaces
{
    public interface IOrderService
    {
        Task<OrderModel> AddOrderAsync(CreateOrderModel createOrderModel, int storageId, int pharmacistId);
        Task<List<OrderModel>> GetOrdersAsync(int storageId = 0);
    }
}
