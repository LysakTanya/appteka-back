﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Appteka.Domain.Models;
using Appteka.Domain.Models.ShiftModels;

namespace Appteka.Domain.Services.Interfaces
{
    public interface IShiftService
    {
        Task<ShiftModel> AddShiftAsync(CreateShiftModel createShiftModel);
        Task DeleteShiftAsync(int shiftId);
        Task<List<ShiftModel>> GetShiftsForWeek(int relativeWeekOffset = 0);
        Task<List<ShiftModel>> GetShiftsForWorker(int workerId, int weekOffset = 0);
        Task<List<WorkerModel>> GetAvailableWorkersAsync(DateTime startDate, int hours, int storageId);
    }
}
