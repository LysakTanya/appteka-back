﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Appteka.Domain.Models.StorageModels;

namespace Appteka.Domain.Services.Interfaces
{
    public interface IStorageService
    {
        Task<StorageModel> AddStorageAsync(StorageModel storageModel);
        Task<StorageModel> UpdateStorageAsync(StorageModel storageModel);
        Task<List<StorageModel>> GetAllStoragesAsync();
        Task<StorageInfoModel> GetStorageInfoByIdAsync(int storageId);
        Task<IEnumerable<MedicineStorageModel>> GetMedicinesFromStorageAsync(int storageId);
    }
}
