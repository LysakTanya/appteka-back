﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Appteka.Domain.Models.SupplierModels;

namespace Appteka.Domain.Services.Interfaces
{
    public interface ISupplierService
    {
        Task<SupplierModel> AddSupplierAsync(SupplierModel supplierModel);
        Task<List<SupplierModel>> GetAllSuppliersAsync();
        Task<SupplierModel> UpdateSupplierAsync(SupplierModel supplierModel);
    }
}
