﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Appteka.Domain.Models.SupplyModels;

namespace Appteka.Domain.Services.Interfaces
{
    public interface ISupplyService
    {
        Task<SupplyModel> AddSupplyAsync(CreateSupplyModel createSupplyModel, int storageId);
        Task<List<SupplyModel>> GetAllSuppliesAsync(int storageId);
    }
}
