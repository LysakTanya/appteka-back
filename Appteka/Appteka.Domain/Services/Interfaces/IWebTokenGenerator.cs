﻿using Appteka.Data.Entities;

namespace Appteka.Domain.Services.Interfaces
{
    public interface IWebTokenGenerator
    {
        string GenerateWebToken(Worker worker);
    }
}
