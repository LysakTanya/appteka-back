﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Appteka.Data.Entities;
using Appteka.Domain.Models;
using Appteka.Domain.Models.WorkerModels;

namespace Appteka.Domain.Services.Interfaces
{
    public interface IWorkerService
    {
        Task<WorkerModel> AddWorkerAsync(CreateWorkerModel createWorkerModel);
        Task<WorkerModel> UpdateWorkerAsync(UpdateWorkerModel updateWorkerModel);
        Task<WorkerModel> UpdateWorkerRoleAsync(int workerId, int roleId);
        Task<WorkerModel> UpdateWorkerStorageAsync(int workerId, int storageId);
        ValueTask<Worker> GetWorkerByIdAsync(int id);
        Task<List<Worker>> GetWorkersByStorageId(int storageId);
        Task<List<WorkerModel>> GetWorkersAsync();
    }
}
