﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Appteka.Data.Entities;
using Appteka.Domain.Models;

namespace Appteka.Domain.Services.Interfaces
{
    public interface IWorkerTypeService
    {
        Task<WorkerTypeModel> AddWorkerTypeAsync(WorkerTypeModel workerTypeModel);
        Task<WorkerType> UpdateHourRateAsync(int workerTypeId, int hourRate);
        Task<WorkerTypeModel> UpdateWorkerTypeAsync(WorkerTypeModel workerTypeModel);
        Task<List<WorkerTypeModel>> GetAllWorkerTypes();
    }
}
