﻿using Appteka.Domain.Models;
using FluentValidation;

namespace Appteka.Domain.Validators.CategoryValidators
{
    public class CategoryModelValidator : AbstractValidator<CategoryModel>
    {
        public CategoryModelValidator()
        {
            RuleFor(c => c.Measure).NotEmpty().NotNull().MaximumLength(20);
            RuleFor(c => c.Name).NotEmpty().NotNull().MinimumLength(2).MaximumLength(50);
        }
    }
}
