﻿using System;
using Appteka.Domain.Models.CustomerModels;
using FluentValidation;

namespace Appteka.Domain.Validators.CustomerValidators
{
    public class CustomerModelValidator : AbstractValidator<CustomerModel>
    {
        public CustomerModelValidator()
        {
            RuleFor(c => c.Name).NotEmpty().NotNull().MinimumLength(2).MaximumLength(50);
            RuleFor(c => c.Surname).NotEmpty().NotNull().MinimumLength(2).MaximumLength(50);
            RuleFor(c => c.CardNumber).NotEmpty().NotNull().Length(10);
            RuleFor(c => c.PhoneNumber).NotEmpty().NotEmpty().Length(13);
            RuleFor(c => c.BirthDate).NotNull().LessThanOrEqualTo(DateTime.Now);
        }
    }
}
