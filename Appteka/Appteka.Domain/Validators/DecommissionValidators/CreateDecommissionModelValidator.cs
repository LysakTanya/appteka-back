﻿using Appteka.Domain.Models.DecommissionModels;
using FluentValidation;

namespace Appteka.Domain.Validators.DecommissionValidators
{
    public class CreateDecommissionModelValidator : AbstractValidator<CreateDecommissionModel>
    {
        public CreateDecommissionModelValidator()
        {
            RuleForEach(dm => dm.MedicineDecommissions)
                .SetValidator(new CreateMedicineDecommissionModelValidator());
        }
    }
}
