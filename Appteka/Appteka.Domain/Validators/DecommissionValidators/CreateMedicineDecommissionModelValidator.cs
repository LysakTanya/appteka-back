﻿using Appteka.Domain.Models.DecommissionModels;
using FluentValidation;

namespace Appteka.Domain.Validators.DecommissionValidators
{
    public class CreateMedicineDecommissionModelValidator : AbstractValidator<CreateMedicineDecommissionModel>
    {
        public CreateMedicineDecommissionModelValidator()
        {
            RuleFor(md => md.Count).NotNull().GreaterThan(0);
            RuleFor(md => md.Price).NotNull().GreaterThan(0);
            RuleFor(md => md.MedicineId).NotNull().GreaterThan(0);
        }
    }
}
