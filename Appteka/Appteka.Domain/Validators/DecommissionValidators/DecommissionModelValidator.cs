﻿using System;
using Appteka.Domain.Models.DecommissionModels;
using Appteka.Domain.Validators.StorageValidators;
using FluentValidation;

namespace Appteka.Domain.Validators.DecommissionValidators
{
    public class DecommissionModelValidator : AbstractValidator<DecommissionModel>
    {
        public DecommissionModelValidator()
        {
            RuleFor(d => d.Date).NotNull().LessThanOrEqualTo(DateTime.Now);
            RuleFor(d => d.Price).NotNull().GreaterThan(0);
            RuleForEach(d => d.MedicineDecommissions).SetValidator(new MedicineDecommissionModelValidator());
            RuleFor(d => d.Storage).SetValidator(new StorageModelValidator());
        }
    }
}
