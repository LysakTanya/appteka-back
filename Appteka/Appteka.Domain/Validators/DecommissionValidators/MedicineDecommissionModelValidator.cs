﻿using Appteka.Domain.Models.DecommissionModels;
using Appteka.Domain.Validators.MedicineValidators;
using FluentValidation;

namespace Appteka.Domain.Validators.DecommissionValidators
{
    public class MedicineDecommissionModelValidator : AbstractValidator<MedicineDecommissionModel>
    {
        public MedicineDecommissionModelValidator()
        {
            RuleFor(md => md.Count).NotNull().GreaterThan(0);
            RuleFor(md => md.Price).NotNull().GreaterThan(0);
            RuleFor(md => md.Medicine).SetValidator(new MedicineModelValidator());
        }
    }
}
