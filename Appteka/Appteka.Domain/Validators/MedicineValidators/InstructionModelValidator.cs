﻿using Appteka.Domain.Models.MedicineModels;
using FluentValidation;

namespace Appteka.Domain.Validators.MedicineValidators
{
    public class InstructionModelValidator : AbstractValidator<InstructionModel>
    {
        public InstructionModelValidator()
        {
            RuleFor(i => i.Dosing).NotEmpty().MinimumLength(2).MaximumLength(2000);
            RuleFor(i => i.Precautions).NotEmpty().MinimumLength(2).MaximumLength(2000);
            RuleFor(i => i.ProperUse).NotEmpty().MinimumLength(2).MaximumLength(2000);
            RuleFor(i => i.SideEffects).NotEmpty().MinimumLength(2).MaximumLength(2000);
            RuleFor(i => i.Storage).NotEmpty().MinimumLength(2).MaximumLength(2000);
            RuleFor(i => i.Interactions).NotEmpty().MinimumLength(2).MaximumLength(2000);
        }
    }
}
