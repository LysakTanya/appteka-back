﻿using Appteka.Domain.Models.MedicineModels;
using FluentValidation;

namespace Appteka.Domain.Validators.MedicineValidators
{
    public class StorageMedicineModelValidator : AbstractValidator<StorageMedicineModel>
    {
        public StorageMedicineModelValidator()
        {
            RuleFor(sm => sm.Count).GreaterThanOrEqualTo(0);
            RuleFor(sm => sm.Medicine).SetValidator(new MedicineModelValidator());
        }
    }
}
