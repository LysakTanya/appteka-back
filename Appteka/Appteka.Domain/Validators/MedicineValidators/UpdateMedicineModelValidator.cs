﻿using Appteka.Domain.Models.MedicineModels;
using FluentValidation;

namespace Appteka.Domain.Validators.MedicineValidators
{
    public class UpdateMedicineModelValidator : AbstractValidator<UpdateMedicineModel>
    {
        public UpdateMedicineModelValidator()
        {
            RuleFor(m => m.Code).NotNull().Length(13).Matches(@"^[0-9]{13}$");
            RuleFor(m => m.IsActual).NotNull();
            RuleFor(m => m.Name).NotNull().NotEmpty().MaximumLength(50).MinimumLength(2);
            RuleFor(m => m.Price).NotNull().GreaterThan(0);
            RuleFor(m => m.Quantity).NotNull().GreaterThan(0);
            RuleFor(m => m.CategoryId).NotNull().GreaterThanOrEqualTo(0);
        }
    }
}
