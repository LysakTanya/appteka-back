﻿using Appteka.Domain.Models.OrderModels;
using FluentValidation;

namespace Appteka.Domain.Validators.OrderValidators
{
    public class CreateMedicineOrderModelValidator : AbstractValidator<CreateMedicineOrderModel>
    {
        public CreateMedicineOrderModelValidator()
        {
            RuleFor(mo => mo.Count).NotNull().GreaterThan(0);
            RuleFor(mo => mo.Price).NotNull().GreaterThan(0);
            RuleFor(mo => mo.MedicineId).GreaterThan(0);
        }
    }
}
