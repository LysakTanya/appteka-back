﻿using Appteka.Domain.Models.OrderModels;
using FluentValidation;

namespace Appteka.Domain.Validators.OrderValidators
{
    public class CreateOrderModelValidator : AbstractValidator<CreateOrderModel>
    {
        public CreateOrderModelValidator()
        {
            RuleFor(o => o.CustomerId).GreaterThan(0);
            RuleForEach(o => o.MedicineOrders).SetValidator(new CreateMedicineOrderModelValidator());
        }
    }
}
