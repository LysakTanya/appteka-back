﻿using Appteka.Domain.Models.OrderModels;
using Appteka.Domain.Validators.MedicineValidators;
using FluentValidation;

namespace Appteka.Domain.Validators.OrderValidators
{
    public class MedicineOrderModelValidator : AbstractValidator<MedicineOrderModel>
    {
        public MedicineOrderModelValidator()
        {
            RuleFor(mo => mo.Medicine).SetValidator(new MedicineModelValidator());
            RuleFor(mo => mo.Count).NotNull().GreaterThan(0);
            RuleFor(mo => mo.Price).NotNull().GreaterThan(0);
        }
    }
}
