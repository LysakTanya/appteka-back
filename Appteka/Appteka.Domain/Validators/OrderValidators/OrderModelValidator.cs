﻿using System;
using Appteka.Domain.Models.OrderModels;
using Appteka.Domain.Validators.CustomerValidators;
using Appteka.Domain.Validators.StorageValidators;
using Appteka.Domain.Validators.WorkerValidator;
using FluentValidation;

namespace Appteka.Domain.Validators.OrderValidators
{
    public class OrderModelValidator : AbstractValidator<OrderModel>
    {
        public OrderModelValidator()
        {
            RuleFor(o => o.Customer).SetValidator(new CustomerModelValidator());
            RuleFor(o => o.Date).NotNull().LessThanOrEqualTo(DateTime.Now);
            RuleFor(o => o.Pharmacist).SetValidator(new WorkerModelValidator());
            RuleFor(o => o.Price).NotNull().GreaterThan(0);
            RuleForEach(o => o.MedicineOrders).SetValidator(new MedicineOrderModelValidator());
            RuleFor(o => o.Storage).SetValidator(new StorageModelValidator());
        }
    }
}
