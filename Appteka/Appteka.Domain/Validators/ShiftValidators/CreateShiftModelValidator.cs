﻿using Appteka.Domain.Models.ShiftModels;
using FluentValidation;

namespace Appteka.Domain.Validators.ShiftValidators
{
    public class CreateShiftModelValidator : AbstractValidator<CreateShiftModel>
    {
        public CreateShiftModelValidator()
        {
            RuleFor(s => s.StartDate).NotNull().LessThan(s => s.EndDate);
            RuleFor(s => s.EndDate).NotNull().GreaterThan(s => s.StartDate);
            RuleFor(s => s.IsDouble).NotNull();
            RuleFor(s => s.WorkerId).NotNull().GreaterThan(0);
        }
    }
}
