﻿using Appteka.Domain.Models.ShiftModels;
using Appteka.Domain.Validators.WorkerValidator;
using FluentValidation;

namespace Appteka.Domain.Validators.ShiftValidators
{
    public class ShiftModelValidator : AbstractValidator<ShiftModel>
    {
        public ShiftModelValidator()
        {
            RuleFor(s => s.StartDate).NotNull().LessThan(s => s.EndDate);
            RuleFor(s => s.EndDate).NotNull().GreaterThan(s => s.StartDate);
            RuleFor(s => s.IsDouble).NotNull();
            RuleFor(s => s.Worker).SetValidator(new WorkerModelValidator());
        }
    }
}
