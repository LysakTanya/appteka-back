﻿using Appteka.Domain.Models.StorageModels;
using Appteka.Domain.Validators.MedicineValidators;
using FluentValidation;

namespace Appteka.Domain.Validators.StorageValidators
{
    public class MedicineStorageModelValidator : AbstractValidator<MedicineStorageModel>
    {
        public MedicineStorageModelValidator()
        {
            RuleFor(ms => ms.Count).NotNull().GreaterThanOrEqualTo(0);
            RuleFor(ms => ms.Medicine).SetValidator(new MedicineModelValidator());
        }
    }
}
