﻿using Appteka.Domain.Models.StorageModels;
using Appteka.Domain.Validators.MedicineValidators;
using Appteka.Domain.Validators.WorkerValidator;
using FluentValidation;

namespace Appteka.Domain.Validators.StorageValidators
{
    public class StorageInfoModelValidator : AbstractValidator<StorageInfoModel>
    {
        public StorageInfoModelValidator()
        {
            RuleFor(s => s.City).NotEmpty().NotNull().MaximumLength(2).MaximumLength(50);
            RuleFor(s => s.Street).NotEmpty().NotNull().MaximumLength(2).MaximumLength(50);
            RuleFor(s => s.Number).NotNull().GreaterThan(0);
            RuleForEach(s => s.Workers).SetValidator(new WorkerModelValidator());
            RuleForEach(s => s.Medicines).SetValidator(new StorageMedicineModelValidator());
        }
    }
}
