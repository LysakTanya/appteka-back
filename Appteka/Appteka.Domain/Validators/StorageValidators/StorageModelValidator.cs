﻿using Appteka.Domain.Models.StorageModels;
using FluentValidation;

namespace Appteka.Domain.Validators.StorageValidators
{
    public class StorageModelValidator : AbstractValidator<StorageModel>
    {
        public StorageModelValidator()
        {
            RuleFor(s => s.City).NotEmpty().NotNull().MaximumLength(2).MaximumLength(50);
            RuleFor(s => s.Street).NotEmpty().NotNull().MaximumLength(2).MaximumLength(50);
            RuleFor(s => s.Number).NotNull().GreaterThan(0);
        }
    }
}
