﻿using Appteka.Domain.Models.SupplierModels;
using FluentValidation;

namespace Appteka.Domain.Validators.SupplierValidators
{
    public class SupplierModelValidator : AbstractValidator<SupplierModel>
    {
        public SupplierModelValidator()
        {
            RuleFor(s => s.Email).EmailAddress().NotEmpty().NotNull().MinimumLength(2).MaximumLength(50);
            RuleFor(s => s.City).NotEmpty().NotNull().MinimumLength(2).MaximumLength(50);
            RuleFor(s => s.Name).NotEmpty().NotNull().MaximumLength(50).MinimumLength(2);
            RuleFor(s => s.PhoneNumber).NotEmpty().NotNull().Length(13);
            RuleFor(s => s.Representative).NotEmpty().NotNull().MinimumLength(2).MaximumLength(200);
        }
    }
}
