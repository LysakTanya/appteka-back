﻿using Appteka.Domain.Models.SupplyModels;
using FluentValidation;

namespace Appteka.Domain.Validators.SupplyValidators
{
    public class CreateMedicineSupplyModelValidator : AbstractValidator<CreateMedicineSupplyModel>
    {
        public CreateMedicineSupplyModelValidator()
        {
            RuleFor(m => m.Count).NotNull().GreaterThan(0);
            RuleFor(m => m.Price).NotNull().GreaterThan(0);
            RuleFor(m => m.MedicineId).NotNull().GreaterThan(0);
        }
    }
}
