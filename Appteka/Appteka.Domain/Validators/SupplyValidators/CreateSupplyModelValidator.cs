﻿using Appteka.Domain.Models.SupplyModels;
using FluentValidation;

namespace Appteka.Domain.Validators.SupplyValidators
{
    public class CreateSupplyModelValidator : AbstractValidator<CreateSupplyModel>
    {
        public CreateSupplyModelValidator()
        {
            RuleFor(s => s.StorageId).NotNull().GreaterThan(0);
            RuleFor(s => s.SupplierId).NotNull().GreaterThan(0);
            RuleForEach(s => s.MedicineSupplies).SetValidator(new CreateMedicineSupplyModelValidator());
        }
    }
}
