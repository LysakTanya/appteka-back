﻿using Appteka.Domain.Models.SupplyModels;
using Appteka.Domain.Validators.MedicineValidators;
using FluentValidation;

namespace Appteka.Domain.Validators.SupplyValidators
{
    public class MedicineSupplyModelValidator : AbstractValidator<MedicineSupplyModel>
    {
        public MedicineSupplyModelValidator()
        {
            RuleFor(s => s.Count).NotNull().GreaterThan(0);
            RuleFor(s => s.Price).NotNull().GreaterThan(0);
            RuleFor(s => s.Medicine).SetValidator(new MedicineModelValidator());
        }
    }
}
