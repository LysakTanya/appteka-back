﻿using System;
using Appteka.Domain.Models.SupplyModels;
using Appteka.Domain.Validators.StorageValidators;
using Appteka.Domain.Validators.SupplierValidators;
using FluentValidation;

namespace Appteka.Domain.Validators.SupplyValidators
{
    public class SupplyModelValidator : AbstractValidator<SupplyModel>
    {
        public SupplyModelValidator()
        {
            RuleFor(s => s.Date).LessThanOrEqualTo(DateTime.Now);
            RuleForEach(s => s.Medicines).SetValidator(new MedicineSupplyModelValidator());
            RuleFor(s => s.Price).NotNull().GreaterThan(0);
            RuleFor(s => s.Storage).SetValidator(new StorageModelValidator());
            RuleFor(s => s.Supplier).SetValidator(new SupplierModelValidator());
        }
    }
}
