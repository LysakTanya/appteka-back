﻿using Appteka.Domain.Models.UserModels;
using FluentValidation;

namespace Appteka.Domain.Validators.UserValidators
{
    public class LoginModelValidator: AbstractValidator<LoginModel>
    {
        public LoginModelValidator()
        {
            RuleFor(l => l.Password).NotNull().NotEmpty().MinimumLength(8);
            RuleFor(l => l.Email).EmailAddress().NotEmpty().NotNull().MinimumLength(2).MaximumLength(50);
        }
    }
}
