﻿using System;
using Appteka.Domain.Models.UserModels;
using FluentValidation;

namespace Appteka.Domain.Validators.UserValidators
{
    public class UserModelValidator: AbstractValidator<AuthenticatedUserModel>
    {
        public UserModelValidator()
        {
            RuleFor(u => u.BirthDate).LessThan(DateTime.Now);
            RuleFor(u => u.HireDate).LessThanOrEqualTo(DateTime.Now);
            RuleFor(u => u.Email).EmailAddress().NotNull().NotEmpty().MinimumLength(2).MaximumLength(50);
            RuleFor(u => u.Name).NotNull().NotEmpty().MinimumLength(2).MaximumLength(50);
            RuleFor(u => u.PhoneNumber).NotEmpty().NotNull().Length(13);
        }
    }
}
