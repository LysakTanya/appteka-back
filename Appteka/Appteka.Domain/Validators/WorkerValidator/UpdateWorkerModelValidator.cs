﻿using System;
using Appteka.Domain.Models.WorkerModels;
using FluentValidation;

namespace Appteka.Domain.Validators.WorkerValidator
{
    public class UpdateWorkerModelValidator : AbstractValidator<UpdateWorkerModel>

    {
        public UpdateWorkerModelValidator()
        {
            RuleFor(w => w.BirthDate).NotNull().LessThan(DateTime.Now);
            RuleFor(w => w.StorageId).NotNull().GreaterThan(0);
            RuleFor(w => w.Name).NotEmpty().NotNull().MinimumLength(2).MaximumLength(50);
            RuleFor(w => w.Surname).NotEmpty().NotNull().MinimumLength(2).MaximumLength(50);
            RuleFor(w => w.Password).NotEmpty().NotNull().MinimumLength(8).MaximumLength(50);
            RuleFor(w => w.HireDate).NotNull().LessThanOrEqualTo(DateTime.Now);
            RuleFor(w => w.TypeId).NotNull().GreaterThan(0);
        }
    }
}
