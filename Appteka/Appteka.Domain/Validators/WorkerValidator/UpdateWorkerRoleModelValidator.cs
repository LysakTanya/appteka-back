﻿using Appteka.Domain.Models.WorkerModels;
using FluentValidation;

namespace Appteka.Domain.Validators.WorkerValidator
{
    public class UpdateWorkerRoleModelValidator : AbstractValidator<UpdateWorkerRoleModel>
    {
        public UpdateWorkerRoleModelValidator()
        {
            RuleFor(w => w.RoleId).NotNull().GreaterThan(0);
            RuleFor(w => w.WorkerId).NotNull().GreaterThan(0);
        }
    }
}
