﻿using Appteka.Domain.Models.WorkerModels;
using FluentValidation;

namespace Appteka.Domain.Validators.WorkerValidator
{
    public class UpdateWorkerStorageModelValidator : AbstractValidator<UpdateWorkerStorageModel>
    {
        public UpdateWorkerStorageModelValidator()
        {
            RuleFor(w => w.StorageId).NotNull().GreaterThan(0);
            RuleFor(w => w.WorkerId).NotNull().GreaterThan(0);
        }
    }
}
