﻿using System;
using Appteka.Domain.Models;
using FluentValidation;

namespace Appteka.Domain.Validators.WorkerValidator
{
    public class WorkerModelValidator : AbstractValidator<WorkerModel>
    {
        public WorkerModelValidator()
        {
            RuleFor(w => w.BirthDate).NotNull().LessThan(DateTime.Now);
            RuleFor(w => w.Email).NotNull().EmailAddress().NotEmpty().MinimumLength(2).MaximumLength(50);
            RuleFor(w => w.HireDate).NotNull().LessThanOrEqualTo(DateTime.Now);
            RuleFor(w => w.StorageId).NotNull().GreaterThan(0);
            RuleFor(w => w.Type).SetValidator(new WorkerTypeModelValidator());
        }
    }
}
