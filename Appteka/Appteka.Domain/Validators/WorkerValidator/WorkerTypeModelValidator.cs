﻿using Appteka.Domain.Models;
using FluentValidation;

namespace Appteka.Domain.Validators.WorkerValidator
{
    public class WorkerTypeModelValidator : AbstractValidator<WorkerTypeModel>
    {
        public WorkerTypeModelValidator()
        {
            RuleFor(wt => wt.Name).NotNull().NotEmpty().MinimumLength(2).MaximumLength(50);
            RuleFor(wt => wt.Salary).NotNull().GreaterThan(0);
        }
    }
}
