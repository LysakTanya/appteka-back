﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;
using Appteka.Domain.Models;
using Appteka.Domain.Services.Interfaces;
using Microsoft.AspNetCore.Authorization;

namespace Appteka.Controllers
{
    [Authorize]
    [Route("api/categories")]
    [ApiController]
    public class CategoryController : ControllerBase
    {
        private readonly ICategoryService categoryService;

        public CategoryController(ICategoryService categoryService)
        {
            this.categoryService = categoryService;
        }

        [HttpGet]
        public async Task<IActionResult> GetAllCategories()
        {
            return Ok(await categoryService.GetAllCategoriesAsync());
        }

        [Authorize(Roles = "Admin")]
        [HttpPost]
        public async Task<IActionResult> AddCategory(CategoryModel categoryModel)
        {
            try
            {
                var newCategory = await categoryService.AddCategoryAsync(categoryModel);
                return Ok(newCategory);
            }
            catch (Exception ex)
            {
                return BadRequest("При додаванні сталась помилка.");
            }
        }

        [Authorize(Roles = "Admin")]
        [HttpPut]
        public async Task<IActionResult> UpdateCategory(CategoryModel categoryModel)
        {
            try
            {
                var newCategory = await categoryService.UpdateCategoryAsync(categoryModel);
                return Ok(newCategory);
            }
            catch (Exception ex)
            {
                return BadRequest("При оновленні сталась помилка.");
            }
        }
    }
}
