﻿using System;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using Appteka.Domain.Models.CustomerModels;
using Appteka.Domain.Services.Interfaces;
using Microsoft.AspNetCore.Authorization;

namespace Appteka.Controllers
{
    [Authorize]
    [Route("api/customers")]
    [ApiController]
    public class CustomerController : ControllerBase
    {
        private readonly ICustomerService customerService;

        public CustomerController(ICustomerService customerService)
        {
            this.customerService = customerService;
        }

        [HttpGet]
        public async Task<IActionResult> GetCustomers()
        {
            return Ok(await customerService.GetCustomersAsync());
        }

        [HttpPost]
        public async Task<IActionResult> AddCustomer(CustomerModel customerModel)
        {
            try
            {
                var customer = await customerService.AddCustomerAsync(customerModel);
                return Ok(customer);
            }
            catch (Exception)
            {
                return BadRequest("Клієнт з таким номер телефону або бонусною карткою вже існує.");
            }
        }

        [HttpGet("card/{cardNumber}")]
        public async Task<IActionResult> GetCustomerByCardNumber(string cardNumber)
        {
            var customer = await customerService.GetCustomerByCardNumberAsync(cardNumber);
            if (customer == null)
                return BadRequest("Клієнт з такими даними не знайдений.");

            return Ok(customer);
        }

        [HttpGet("phone/{phoneNumber}")]
        public async Task<IActionResult> GetCustomerByPhoneNumber(string phoneNumber)
        {
            var customer = await customerService.GetCustomerByPhoneNumberAsync(phoneNumber);
            if (customer == null)
                return BadRequest("Клієнт з такими даними не знайдений.");

            return Ok(customer);
        }
    }
}
