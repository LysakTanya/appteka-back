﻿using System;
using System.Security.Claims;
using System.Threading.Tasks;
using Appteka.Domain.Models.DecommissionModels;
using Appteka.Domain.Services.Interfaces;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace Appteka.Controllers
{
    [Authorize(Roles="Admin, Manager")]
    [Route("api/decommissions")]
    [ApiController]
    public class DecommissionController : ControllerBase
    {
        private readonly IDecommissionService decommissionService;

        public DecommissionController(IDecommissionService decommissionService)
        {
            this.decommissionService = decommissionService;
        }

        [HttpGet]
        public async Task<IActionResult> GetDecommissions()
        {
            return User.IsInRole("Admin") ? Ok(await decommissionService.GetAllDecommissions()) : Ok(await decommissionService.GetAllDecommissions(GetStorageId()));
        }

        [HttpPost]
        public async Task<IActionResult> AddDecommission(CreateDecommissionModel createDecommissionModel)
        {
            try
            {
                var storageId = GetStorageId();
                var decommission = await decommissionService.AddDecommissionAsync(createDecommissionModel, storageId);
                return Ok(decommission);
            }
            catch (Exception)
            {
                return BadRequest("При здійсненні списання виникла помилка.");
            }
        }

        private int GetStorageId()
        {
            return int.Parse(User.FindFirstValue("StorageId"));
        }
    }
}
