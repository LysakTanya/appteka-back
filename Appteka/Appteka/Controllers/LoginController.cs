﻿using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using Appteka.Domain.Models.UserModels;
using Appteka.Domain.Services.Interfaces;
using Microsoft.AspNetCore.Authorization;

namespace Appteka.Controllers
{
    [AllowAnonymous]
    [Route("api/login")]
    [ApiController]
    public class LoginController : ControllerBase
    {
        private readonly ILoginService loginService;

        public LoginController(ILoginService loginService)
        {
            this.loginService = loginService;
        }

        [HttpPost]
        public async Task<IActionResult> Login(LoginModel user)
        {
            var authenticatedUser = await loginService.LoginAsync(user);
            if (authenticatedUser == null)
            {
                return BadRequest();
            }

            return Ok(authenticatedUser);
        }
    }
}
