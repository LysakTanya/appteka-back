﻿using System;
using System.Threading.Tasks;
using Appteka.Domain.Models.MedicineModels;
using Appteka.Domain.Services.Interfaces;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace Appteka.Controllers
{
    [Authorize]
    [Route("api/medicines")]
    [ApiController]
    public class MedicineController : ControllerBase
    {
        private readonly IMedicineService medicineService;

        public MedicineController(IMedicineService medicineService)
        {
            this.medicineService = medicineService;
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> GetMedicineById(int id)
        {
            return Ok(await medicineService.GetMedicineByIdAsync(id));
        }

        [Authorize(Roles = "Admin")]
        [HttpPost]
        public async Task<IActionResult> AddMedicine(CreateMedicineModel createMedicineModel)
        {
            try
            {
                var newMedicine = await medicineService.AddMedicineAsync(createMedicineModel);
                return Ok(newMedicine);
            }
            catch (Exception)
            {
                return BadRequest("Препарат з таким кодом вже існує.");
            }
        }

        [Authorize(Roles = "Admin")]
        [HttpPut]
        public async Task<IActionResult> UpdateMedicine(UpdateMedicineModel medicineModel)
        {
            try
            {
                var medicine = await medicineService.UpdateMedicineAsync(medicineModel);
                return Ok(medicine);
            }
            catch (Exception)
            {
                return BadRequest("При оновленні препарату виникла помилка.");
            }
        }

        [HttpGet]
        public async Task<IActionResult> GetAllMedicines()
        {
            return Ok(await medicineService.GetAllMedicinesAsync());
        }

        [Authorize(Roles = "Admin")]
        [HttpPut("instructions")]
        public async Task<IActionResult> AddInstruction(InstructionModel instruction)
        {
            try
            {
                var medicine = await medicineService.AddInstructionAsync(instruction);
                return Ok(medicine);
            }
            catch (Exception)
            {
                return BadRequest("При додаванні інструкцій виникла помилка.");
            }
        }
    }
}
