﻿using System;
using System.Security.Claims;
using System.Threading.Tasks;
using Appteka.Domain.Models.OrderModels;
using Appteka.Domain.Services.Interfaces;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace Appteka.Controllers
{
    [Authorize]
    [Route("api/orders")]
    [ApiController]
    public class OrderController : ControllerBase
    {
        private readonly IOrderService orderService;

        public OrderController(IOrderService orderService)
        {
            this.orderService = orderService;
        }

        [HttpPost]
        public async Task<IActionResult> AddOrder(CreateOrderModel createOrderModel)
        {
            try
            {
                int storageId = GetStorageId();
                int workerId = GetWorkerId();
                var order = await orderService.AddOrderAsync(createOrderModel, storageId, workerId);
                return Ok(order);
            }
            catch (Exception)
            {
                return BadRequest("При створенні замовлення виникла помилка.");
            }
        }

        private int GetStorageId()
        {
            return int.Parse(User.FindFirstValue("StorageId"));
        }

        private int GetWorkerId()
        {
            return int.Parse(User.FindFirstValue(ClaimTypes.Name));
        }

        [HttpGet]
        public async Task<IActionResult> GetOrders()
        {
            return User.IsInRole("Admin") ? Ok(await orderService.GetOrdersAsync()) : Ok(await orderService.GetOrdersAsync(GetStorageId()));
        }
    }
}
