﻿using System;
using System.Security.Claims;
using System.Threading.Tasks;
using Appteka.Domain.Models.ShiftModels;
using Microsoft.AspNetCore.Mvc;
using Appteka.Domain.Services.Interfaces;
using Microsoft.AspNetCore.Authorization;

namespace Appteka.Controllers
{
    [Authorize]
    [Route("api/shifts")]
    [ApiController]
    public class ShiftController : ControllerBase
    {
        private readonly IShiftService shiftService;

        public ShiftController(IShiftService shiftService)
        {
            this.shiftService = shiftService;
        }

        [Authorize(Roles = "Admin")]
        [HttpGet("weeks/{weekOffset}")]
        public async Task<IActionResult> GetShiftsForWeek(int weekOffset)
        {
            return Ok(await shiftService.GetShiftsForWeek(weekOffset));
        }

        [HttpGet("weeks/{weekOffset}/workers/{workerId}")]
        public async Task<IActionResult> GetShiftsForWorker(int weekOffset, int workerId)
        {
            if (User.FindFirst(u => u.Type == ClaimTypes.Name)?.Value != workerId.ToString())
            {
                return BadRequest("Ви не маєте доступу до змін іншого працівника.");
            }

            return Ok(await shiftService.GetShiftsForWorker(workerId, weekOffset));
        }

        [Authorize(Roles = "Admin")]
        [HttpGet("start/{startDate}/duration/{hours}/storages/{storageId}")]
        public async Task<IActionResult> GetAvailableWorkers(DateTime startDate, int hours, int storageId)
        {
            return Ok(await shiftService.GetAvailableWorkersAsync(startDate, hours, storageId));
        }

        [Authorize(Roles = "Admin")]
        [HttpPost]
        public async Task<IActionResult> AddShift(CreateShiftModel createShiftModel)
        {
            try
            {
                var shift = await shiftService.AddShiftAsync(createShiftModel);
                return Ok(shift);
            }
            catch (Exception)
            {
                return BadRequest("Виникла помилка при додаванні зміни.");
            }
        }

        [Authorize(Roles = "Admin")]
        [HttpDelete("{shiftId}")]
        public async Task<IActionResult> DeleteShift(int shiftId)
        {
            try
            {
                await shiftService.DeleteShiftAsync(shiftId);
                return Ok();
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }
    }
}
