﻿using System;
using System.Security.Claims;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using Appteka.Domain.Models.StorageModels;
using Appteka.Domain.Services.Interfaces;
using Microsoft.AspNetCore.Authorization;

namespace Appteka.Controllers
{
    [Authorize]
    [Route("api/storages")]
    [ApiController]
    public class StorageController : ControllerBase
    {
        private readonly IStorageService storageService;

        public StorageController(IStorageService storageService)
        {
            this.storageService = storageService;
        }

        [Authorize(Roles = "Admin")]
        [HttpPost]
        public async Task<IActionResult> AddStorage([FromBody] StorageModel storageModel)
        {
            try
            {
                var storage = await storageService.AddStorageAsync(storageModel);
                return Ok(storage);
            }
            catch (Exception)
            {
                return BadRequest("При додаванні нової торгової точки виникла помилка.");
            }
        }

        [Authorize(Roles = "Admin")]
        [HttpPut]
        public async Task<IActionResult> UpdateStorage([FromBody] StorageModel storageModel)
        {
            try
            {
                var storage = await storageService.UpdateStorageAsync(storageModel);
                return Ok(storage);
            }
            catch (Exception)
            {
                return BadRequest("При оновленні торгової точки виникла помилка.");
            }
        }

        [Authorize(Roles = "Admin")]
        [HttpGet]
        public async Task<IActionResult> GetAllStorages()
        {
            return Ok(await storageService.GetAllStoragesAsync());
        }

        [Authorize(Roles = "Manager, Admin")]
        [HttpGet("{storageId}")]
        public async Task<IActionResult> GetStorageInfoById(int storageId)
        {
            try
            {
                var storageInfo = await storageService.GetStorageInfoByIdAsync(storageId);
                return Ok(storageInfo);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpGet("medicines")]
        public async Task<IActionResult> GetStorageMedicines()
        {
            try
            {
                var storageId = GetStorageId();
                var medicines = await storageService.GetMedicinesFromStorageAsync(storageId);
                return Ok(medicines);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        private int GetStorageId()
        {
            return int.Parse(User.FindFirstValue("StorageId"));
        }
    }
}
