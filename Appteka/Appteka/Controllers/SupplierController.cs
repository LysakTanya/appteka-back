﻿using System;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using Appteka.Domain.Models.SupplierModels;
using Appteka.Domain.Services.Interfaces;
using Microsoft.AspNetCore.Authorization;

namespace Appteka.Controllers
{
    [Authorize(Roles = "Admin, Manager")]
    [Route("api/suppliers")]
    [ApiController]
    public class SupplierController : ControllerBase
    {
        private readonly ISupplierService supplierService;

        public SupplierController(ISupplierService supplierService)
        {
            this.supplierService = supplierService;
        }

        [HttpGet]
        public async Task<IActionResult> GetSuppliers()
        {
            return Ok(await supplierService.GetAllSuppliersAsync());
        }

        [HttpPost]
        public async Task<IActionResult> AddSupplier(SupplierModel supplierModel)
        {
            try
            {
                var supplier = await supplierService.AddSupplierAsync(supplierModel);
                return Ok(supplier);
            }
            catch (Exception)
            {
                return BadRequest("При додаванні поставника виникла помилка.");
            }
        }

        [HttpPut]
        public async Task<IActionResult> UpdateSupplier(SupplierModel supplierModel)
        {
            try
            {
                var supplier = await supplierService.UpdateSupplierAsync(supplierModel);
                return Ok(supplier);
            }
            catch (Exception)
            {
                return BadRequest("При оновленні поставника виникла помилка.");
            }
        }
    }
}
