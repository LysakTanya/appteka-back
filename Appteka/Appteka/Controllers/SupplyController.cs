﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Security.Claims;
using System.Threading.Tasks;
using Appteka.Domain.Models.SupplyModels;
using Appteka.Domain.Services.Interfaces;
using Microsoft.AspNetCore.Authorization;

namespace Appteka.Controllers
{
    [Authorize(Roles = "Admin, Manager")]
    [Route("api/supplies")]
    [ApiController]
    public class SupplyController : ControllerBase
    {
        private readonly ISupplyService supplyService;

        public SupplyController(ISupplyService supplyService)
        {
            this.supplyService = supplyService;
        }

        [HttpGet]
        public async Task<IActionResult> GetSupplies()
        {
            try
            {
                int storageId = GetStorageId();
                return Ok(await supplyService.GetAllSuppliesAsync(storageId));
            }
            catch (Exception)
            {
                return BadRequest("При отриманні поставок виникла помилка.");
            }
        }

        [HttpPost]
        public async Task<IActionResult> AddSupply(CreateSupplyModel createSupplyModel)
        {
            try
            {
                int storageId = GetStorageId();
                return Ok(await supplyService.AddSupplyAsync(createSupplyModel, storageId));
            }
            catch (Exception)
            {
                return BadRequest("При створенні поставки виникла помилка.");
            }
        }

        private int GetStorageId()
        {
            return int.Parse(User.FindFirstValue("StorageId"));
        }
    }
}
