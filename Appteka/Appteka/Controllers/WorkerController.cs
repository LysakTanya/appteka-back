﻿using System;
using System.Threading.Tasks;
using Appteka.Domain.Models.WorkerModels;
using Microsoft.AspNetCore.Mvc;
using Appteka.Domain.Services.Interfaces;
using Microsoft.AspNetCore.Authorization;

namespace Appteka.Controllers
{
    [Route("api/workers")]
    [ApiController]
    public class WorkerController : ControllerBase
    {
        private readonly IWorkerService workerService;

        public WorkerController(IWorkerService workerService)
        {
            this.workerService = workerService;
        }

        [Authorize(Roles = "Admin")]
        [HttpGet]
        public async Task<IActionResult> GetAllWorkers()
        {
            return Ok(await workerService.GetWorkersAsync());
        }

        [Authorize(Roles = "Admin")]
        [HttpPost]
        public async Task<IActionResult> AddWorker(CreateWorkerModel createWorkerModel)
        {
            try
            {
                var worker = await workerService.AddWorkerAsync(createWorkerModel);
                return Ok(worker);
            }
            catch (Exception)
            {
                return BadRequest("При додаванні працівника сталась помилка.");
            }
        }

        [Authorize(Roles = "Admin")]
        [HttpPut]
        public async Task<IActionResult> UpdateWorker(UpdateWorkerModel updateWorkerModel)
        {
            try
            {
                var worker = await workerService.UpdateWorkerAsync(updateWorkerModel);
                return Ok(worker);
            }
            catch (Exception)
            {
                return BadRequest("При оновленні інформації про працівника сталась помилка.");
            }
        }

        [Authorize(Roles = "Admin")]
        [HttpPut("role")]
        public async Task<IActionResult> UpdateWorkerRole([FromBody] UpdateWorkerRoleModel updateRoleModel)
        {
            try
            {
                var worker =
                    await workerService.UpdateWorkerRoleAsync(updateRoleModel.WorkerId, updateRoleModel.RoleId);
                return Ok(worker);
            }
            catch (Exception)
            {
                return BadRequest("При оновленні інформації про працівника сталась помилка.");
            }
        }

        [Authorize(Roles = "Admin")]
        [HttpPut("storage")]
        public async Task<IActionResult> UpdateWorkerStorage([FromBody] UpdateWorkerStorageModel updateRoleModel)
        {
            try
            {
                var worker =
                    await workerService.UpdateWorkerStorageAsync(updateRoleModel.WorkerId, updateRoleModel.StorageId);
                return Ok(worker);
            }
            catch (Exception)
            {
                return BadRequest("При оновленні інформації про працівника сталась помилка.");
            }
        }
    }
}
