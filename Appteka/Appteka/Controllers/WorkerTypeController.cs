﻿using System;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using Appteka.Domain.Models;
using Appteka.Domain.Services.Interfaces;
using Microsoft.AspNetCore.Authorization;

namespace Appteka.Controllers
{
    [Authorize(Roles = "Admin")]
    [Route("api/worker-types")]
    [ApiController]
    public class WorkerTypeController : ControllerBase
    {
        private readonly IWorkerTypeService workerTypeService;

        public WorkerTypeController(IWorkerTypeService workerTypeService)
        {
            this.workerTypeService = workerTypeService;
        }

        [HttpGet]
        public async Task<IActionResult> GetWorkerTypes()
        {
            return Ok(await workerTypeService.GetAllWorkerTypes());
        }

        [HttpPost]
        public async Task<IActionResult> AddWorkerType(WorkerTypeModel workerTypeModel)
        {
            try
            {
                var workerType = await workerTypeService.AddWorkerTypeAsync(workerTypeModel);
                return Ok(workerType);
            }
            catch (Exception ex)
            {
                return BadRequest("При додаванні нового типу працівника сталась помилка.");
            }
        }

        [HttpPut]
        public async Task<IActionResult> UpdateWorkerType(WorkerTypeModel workerTypeModel)
        {
            try
            {
                var workerType = await workerTypeService.UpdateWorkerTypeAsync(workerTypeModel);
                return Ok(workerType);
            }
            catch (Exception ex)
            {
                return BadRequest("При оновленні типу працівника сталась помилка.");
            }
        }
    }
}
