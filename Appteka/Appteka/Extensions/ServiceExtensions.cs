﻿using System.Text;
using Appteka.Data;
using Appteka.Data.Entities;
using Appteka.Domain.Configurations;
using Appteka.Domain.Models;
using Appteka.Domain.Models.CustomerModels;
using Appteka.Domain.Models.DecommissionModels;
using Appteka.Domain.Models.MedicineModels;
using Appteka.Domain.Models.OrderModels;
using Appteka.Domain.Models.ShiftModels;
using Appteka.Domain.Models.StorageModels;
using Appteka.Domain.Models.SupplierModels;
using Appteka.Domain.Models.SupplyModels;
using Appteka.Domain.Models.UserModels;
using Appteka.Domain.Models.WorkerModels;
using Appteka.Domain.Services.Implementation;
using Appteka.Domain.Services.Interfaces;
using Appteka.Domain.Validators.CategoryValidators;
using Appteka.Domain.Validators.CustomerValidators;
using Appteka.Domain.Validators.DecommissionValidators;
using Appteka.Domain.Validators.MedicineValidators;
using Appteka.Domain.Validators.OrderValidators;
using Appteka.Domain.Validators.ShiftValidators;
using Appteka.Domain.Validators.StorageValidators;
using Appteka.Domain.Validators.SupplierValidators;
using Appteka.Domain.Validators.SupplyValidators;
using Appteka.Domain.Validators.UserValidators;
using Appteka.Domain.Validators.WorkerValidator;
using FluentValidation;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.IdentityModel.Tokens;

namespace Appteka.Extensions
{
    internal static class ServiceExtensions
    {
        public static void AddRepositories(this IServiceCollection services)
        {
            services.AddScoped<IRepository<Medicine>, Repository<Medicine>>();
            services.AddScoped<IRepository<Storage>, Repository<Storage>>();
            services.AddScoped<IRepository<Worker>, Repository<Worker>>();
            services.AddScoped<IRepository<WorkerType>, Repository<WorkerType>>();
            services.AddScoped<IRepository<Decommission>, Repository<Decommission>>();
            services.AddScoped<IRepository<Supply>, Repository<Supply>>();
            services.AddScoped<IRepository<Supplier>, Repository<Supplier>>();
            services.AddScoped<IRepository<Order>, Repository<Order>>();
            services.AddScoped<IRepository<Shift>, Repository<Shift>>();
            services.AddScoped<IRepository<Customer>, Repository<Customer>>();
            services.AddScoped<IRepository<Category>, Repository<Category>>();
        }

        public static void AddServices(this IServiceCollection services)
        {
            services.AddScoped<IMedicineService, MedicineService>();
            services.AddScoped<ILoginService, LoginService>();
            services.AddScoped<IStorageService, StorageService>();
            services.AddScoped<IWorkerService, WorkerService>();
            services.AddScoped<IWorkerTypeService, WorkerTypeService>();
            services.AddScoped<ISupplyService, SupplyService>();
            services.AddScoped<ISupplierService, SupplierService>();
            services.AddScoped<ICategoryService, CategoryService>();
            services.AddScoped<ICustomerService, CustomerService>();
            services.AddScoped<IDecommissionService, DecommissionService>();
            services.AddScoped<IOrderService, OrderService>();
            services.AddScoped<IShiftService, ShiftService>();
            services.AddScoped<IWebTokenGenerator, JwtGenerator>();
        }

        public static void AddValidators(this IServiceCollection services)
        {
            services.AddSingleton<IValidator<MedicineModel>, MedicineModelValidator>();
            services.AddSingleton<IValidator<InstructionModel>, InstructionModelValidator>();
            services.AddSingleton<IValidator<CreateMedicineModel>, CreateMedicineModelValidator>();
            services.AddSingleton<IValidator<UpdateMedicineModel>, UpdateMedicineModelValidator>();
            services.AddSingleton<IValidator<StorageMedicineModel>, StorageMedicineModelValidator>();

            services.AddSingleton<IValidator<CategoryModel>, CategoryModelValidator>();

            services.AddSingleton<IValidator<CustomerModel>, CustomerModelValidator>();

            services.AddSingleton<IValidator<CreateDecommissionModel>, CreateDecommissionModelValidator>();
            services.AddSingleton<IValidator<CreateMedicineDecommissionModel>, CreateMedicineDecommissionModelValidator>();
            services.AddSingleton<IValidator<DecommissionModel>, DecommissionModelValidator>();
            services.AddSingleton<IValidator<MedicineDecommissionModel>, MedicineDecommissionModelValidator>();

            services.AddSingleton<IValidator<CreateMedicineOrderModel>, CreateMedicineOrderModelValidator>();
            services.AddSingleton<IValidator<CreateOrderModel>, CreateOrderModelValidator>();
            services.AddSingleton<IValidator<MedicineOrderModel>, MedicineOrderModelValidator>();
            services.AddSingleton<IValidator<OrderModel>, OrderModelValidator>();

            services.AddSingleton<IValidator<CreateShiftModel>, CreateShiftModelValidator>();
            services.AddSingleton<IValidator<ShiftModel>, ShiftModelValidator>();

            services.AddSingleton<IValidator<MedicineStorageModel>, MedicineStorageModelValidator>();
            services.AddSingleton<IValidator<StorageInfoModel>, StorageInfoModelValidator>();
            services.AddSingleton<IValidator<StorageModel>, StorageModelValidator>();

            services.AddSingleton<IValidator<SupplierModel>, SupplierModelValidator>();

            services.AddSingleton<IValidator<CreateMedicineSupplyModel>, CreateMedicineSupplyModelValidator>();
            services.AddSingleton<IValidator<CreateSupplyModel>, CreateSupplyModelValidator>();
            services.AddSingleton<IValidator<MedicineSupplyModel>, MedicineSupplyModelValidator>();
            services.AddSingleton<IValidator<SupplyModel>, SupplyModelValidator>();

            services.AddSingleton<IValidator<LoginModel>, LoginModelValidator>();
            services.AddSingleton<IValidator<AuthenticatedUserModel>, UserModelValidator>();

            services.AddSingleton<IValidator<CreateWorkerModel>, CreateWorkerModelValidator>();
            services.AddSingleton<IValidator<UpdateWorkerRoleModel>, UpdateWorkerRoleModelValidator>();
            services.AddSingleton<IValidator<UpdateWorkerModel>, UpdateWorkerModelValidator>();
            services.AddSingleton<IValidator<UpdateWorkerStorageModel>, UpdateWorkerStorageModelValidator>();
            services.AddSingleton<IValidator<WorkerModel>, WorkerModelValidator>();
            services.AddSingleton<IValidator<WorkerTypeModel>, WorkerTypeModelValidator>();
        }

        public static void AddBearer(this IServiceCollection services, IConfiguration configuration)
        {
            services.Configure<Jwt>(configuration.GetSection("Jwt"));
            var jwtOptions = configuration.GetSection(nameof(Jwt)).Get<Jwt>();
            services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
                .AddJwtBearer(options =>
                {
                    options.TokenValidationParameters = new TokenValidationParameters
                    {
                        ValidateIssuer = true,
                        ValidateAudience = true,
                        ValidateLifetime = true,
                        ValidateIssuerSigningKey = true,
                        ValidIssuer = jwtOptions.Issuer,
                        ValidAudience = jwtOptions.Issuer,
                        IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(jwtOptions.Key)),
                    };
                });
        }
    }
}
